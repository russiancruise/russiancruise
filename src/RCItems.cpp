#include "RCItems.h"

RCItems*
RCItems::self = nullptr;

RCItems*
RCItems::getInstance()
{
    if(!self)
        self = new RCItems();

    return self;
}

RCItems::RCItems()
{
    ClassName = "RCItems";
}

RCItems::~RCItems()
{
    //dtor
}

int
RCItems::init(const char* Dir)
{
    strcpy(RootDir, Dir);

    insim = CInsim::getInstance();

    return 0;
}


void
RCItems::Event()
{

}

void
RCItems::InsimNCN(struct IS_NCN *packet)
{

    players[packet->UCID].UName = packet->UName;
    players[packet->UCID].Admin = packet->Admin;

    if(!packet->Admin)
    {
        //players[packet->UCID].Admin = isAdmin(packet->UName);
    }

}

void
RCItems::InsimCNL(struct IS_CNL *packet)
{
    players.erase(packet->UCID);
}

void
RCItems::InsimMCI(struct IS_MCI *packet)
{
    for(int i = 0; i < packet->NumC; ++i)
    {
//        byte UCID = PLIDtoUCID[ packet->Info[i].PLID ];
//        int X = packet->Info[i].X / 65536;
//        int Y = packet->Info[i].Y / 65536;
    }
}

void
RCItems::InsimNPL(struct IS_NPL *packet)
{
    PLIDtoUCID[packet->PLID] = packet->UCID;
}

void
RCItems::InsimPLL( struct IS_PLL* packet )
{
    PLIDtoUCID.erase(packet->PLID);
}

void
RCItems::InsimPLP( struct IS_PLP* packet )
{
//    PLIDtoUCID.erase(packet->PLID);
}

void
RCItems::InsimMSO( struct IS_MSO* packet )
{
    //TODO: Just Do It
    // https://www.youtube.com/watch?v=ZXsQAXx_ao0

    if (packet->UserType != MSO_PREFIX)
        return;

    byte UCID = packet->UCID;

    xString Message = packet->Msg + packet->TextStart;
    vector<xString> args = Message.split(' ',1);

    if(Message == "!items")
    {
        // show all menu
    }

    if(args.size() < 2)
        return;

    if(args[0] != "!items")
        return;

    if (args[1] == "reload" && (players.at(UCID).Admin || UCID == 0) )
    {
        Items::Get()->LoadItems();

        for (auto& plr: players)
            if(plr.second.Admin)
                insim->SendMTC(plr.first, RCMessage::getInstance()->_(plr.first, "ITEMS_RELOADED"));

        return;
    }
}

