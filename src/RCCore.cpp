#include "RCCore.h"

RCCore*
RCCore::self = nullptr;

string
RCCore::IS_PRODUCT_NAME = "";

RCCore*
RCCore::getInstance()
{
    if(!self)
        self = new RCCore();

    return self;
}

RCCore::RCCore()
{
    ClassName = "RCCore";
}

RCCore::~RCCore()
{
}

int
RCCore::init(const char* Dir)
{
    strcpy(this->RootDir,Dir);

    insim = CInsim::getInstance();
    db = DBMySQL::Get();
    msg = RCMessage::getInstance();
    bank = RCBank::getInstance();
    police = RCPolice::getInstance();
    street = RCStreet::getInstance();
    nrg = RCEnergy::getInstance();
    dl = RCDL::getInstance();

    CCText("^3"+ClassName+":\t\t^2inited");
    return 0;
}

void
RCCore::InsimBFN( struct IS_BFN* packet )
{
    time_t now;
    time(&now);

    if ((now - players.at(packet->UCID).LastBFN) < 5)
        return;

    players.at(packet->UCID).LastBFN = now;
    InfoPage(packet->UCID, 1);
}

void
RCCore::InsimBTC( struct IS_BTC* packet )
{
    byte UCID = packet->UCID;

    /** ������������ ������� �� ������� ������������ **/
    if (packet->ClickID<69)
    {
        this->ShowUsersList(packet->UCID);

        byte L = 23, T = 187;
        string uname = StringFormat(msg->_( packet->UCID, "MsgPlFor" ), players.at(packet->ReqI).PName.c_str());

        if (packet->ReqI == UCID)
            uname = string(msg->_(UCID, "ItsYou"));

        if (players.size()>24)
            L = 45;

        #ifdef _RC_POLICE_H
        if (police->IsCop(UCID))
        {
            uname += StringFormat(" ^8(%s^8)", players.at(packet->ReqI).UName.c_str());
            if (!police->IsCop(packet->ReqI))
                T = 175;
        }
        #endif

        insim->SendButton(255, UCID, 70, L, T, 24, 4, 32 + 64, uname); // �� ������ ������ ������

        byte PlFlag = 3+16+8;   // BStyle �������� ������

        if (packet->ReqI == UCID)
            PlFlag = 7 + 16; // ����� �� ����� ����������������� ��� � �����

        insim->SendButton(packet->ReqI, UCID, 71, L, T+4, 24, 4, PlFlag, msg->_( packet->UCID, "1000" ), 7);   // �������� ������
        insim->SendButton(packet->ReqI, UCID, 72, L, T+8, 24, 4, PlFlag, msg->_( packet->UCID, "1001" ), 82);  // �������� ���

        #ifdef _RC_POLICE_H
        if ( police->IsCop(UCID) && !police-> IsCop(packet->ReqI) && packet->ReqI != UCID)
        {
            PlFlag = 32 + 8 + 3;
            insim->SendButton(packet->ReqI, UCID, 73, L, T+12, 24, 4, PlFlag, msg->_( UCID, "FinesButton" ));   //�����

            if (police->GetCopRank(UCID) <= 2)
                PlFlag = 32 + 7;

            insim->SendButton(packet->ReqI, UCID, 74, L, T+16, 24, 4, PlFlag, msg->_( UCID, "1004" ));          //������ ���

            if (police->GetCopRank(UCID) == 2)
                PlFlag = 32 + 8 + 3;

            insim->SendButton(packet->ReqI, UCID, 75, L, T+20, 24, 4, PlFlag, msg->_( UCID, "1005" ));          //������ ����
        }
        else
        {
            insim->SendBFN(UCID, 73, 75);


        }
        #endif
    }

    /** ������ ������ � �������������� **/
    if (packet->ClickID==69)
        insim->SendBFN(packet->UCID, 21, 80);


    if (packet->ReqI==255)
    {
        if (packet->ClickID == 180)
            InfoPage(packet->UCID, 1);
        if (packet->ClickID == 181)
            InfoPage(packet->UCID, 2);
        if (packet->ClickID == 182)
            InfoPage(packet->UCID, 3);
        if (packet->ClickID == 183)
            InfoPage(packet->UCID, 4);
    }
}

void
RCCore::InsimBTT( struct IS_BTT* packet )
{
    /**  ������������ �������� ������   */
    if (packet->ClickID==71)
    {
        if (atoi(packet->Text) > 0)
        {
            if (bank->GetCash(packet->UCID) < atoi(packet->Text))
            {
                insim->SendMTC(packet->UCID, msg->_( packet->UCID, "1101" ));
                return;
            }

            string Msg;
            Msg = StringFormat(msg->_(packet->ReqI, "GetMoney" ), players.at(packet->UCID).PName.c_str(), atoi(packet->Text));
            insim->SendMTC(packet->ReqI, Msg);
            msg->AddNotify(packet->ReqI, Msg);

            Msg = StringFormat(msg->_(packet->UCID, "SendMoney" ), players.at(packet->ReqI).PName.c_str(), atoi(packet->Text));
            insim->SendMTC(packet->UCID, Msg);

            bank->RemCash(packet->UCID, atoi(packet->Text));
            bank->AddCash(packet->ReqI, atoi(packet->Text), false);

            CLog::log("RCCore", "send",StringFormat("send %s => %s (%s RUR)",players.at(packet->UCID).UName.c_str(), players.at(packet->ReqI).UName.c_str(), packet->Text));
        }
    }

    /**  ������������ �������� ���������  */
    if (packet->ClickID == 72)
    {
        if (strlen(packet->Text) > 0)
        {
            string Msg;
            Msg = StringFormat(msg->_( packet->ReqI, "MsgFrom" ), players.at(packet->UCID).PName.c_str(), packet->Text );
            insim->SendMTC(packet->ReqI, Msg);
            msg->AddNotify(packet->ReqI, Msg);

            RCBaseClass::CCText("^3" + (string)players.at(packet->UCID).UName + " ^7to ^3" + (string)players.at(packet->ReqI).UName + "^7: " + RCBaseClass::StripText(packet->Text));
        }
    }
}

void
RCCore::InsimCNL( struct IS_CNL* packet )
{
    RCBaseClass::CCText("^5<< disconnected " + string(players.at(packet->UCID).UName) + " (" + string(RCBaseClass::GetReason(packet->Reason) ) + ")");

    // ����� �������
    // � ������� ����� � ������� ������ ������ ������� (��������� �� ������ � ����� ������)
    // ���
    // ���� � ������� ������ ������� ����� � ��� ����� ��� �� �������
    // �.�. ������ ����� �������� ������ ��������� ���������� ������, ������ ���������
    if (packet->Reason != LEAVR_DISCO &&
        ((time(NULL) - players.at(packet->UCID).PLL_time) < 1)
        ||
        ((time(NULL) - players.at(packet->UCID).PLL_time) >= 1 && bonuses[players.at(packet->UCID).UName]["dist"].asInt() > 0)
        )
    {
        if(bonuses[players.at(packet->UCID).UName].isMember("backup"))
        {
            bonuses[players.at(packet->UCID).UName]["count"] = bonuses[players.at(packet->UCID).UName]["backup"]["count"];
            bonuses[players.at(packet->UCID).UName]["dist"] = bonuses[players.at(packet->UCID).UName]["backup"]["dist"];
        }
        bonuses[players.at(packet->UCID).UName]["time"] = (u_int)time(NULL);
    }

    // ����� ����� ������ "�����"
    else
    {
        bonuses[players.at(packet->UCID).UName]["time"] = 0;      // �������� �����
    }

    if(bonuses[players.at(packet->UCID).UName].isMember("backup"))
        bonuses[players.at(packet->UCID).UName].removeMember("backup");

    Save( packet->UCID );

    players.erase(packet->UCID);
}

void
RCCore::InsimTOC( struct IS_TOC* packet )
{
    insim->SendMTC(packet->NewUCID, "^1Access denied");
    insim->SendMST("/spec " + string(players.at(packet->NewUCID).UName));
}

void
RCCore::InsimCPR( struct IS_CPR* packet )
{
    players.at(packet->UCID).PName = packet->PName;
}

void
RCCore::InsimMCI( struct IS_MCI* packet )
{
    for (int i = 0; i < packet->NumC; i++)
    {
        if(PLIDtoUCID.find(packet->Info[i].PLID) == PLIDtoUCID.end())
            continue;

        byte UCID = PLIDtoUCID[packet->Info[i].PLID];

        if(players.find(UCID) == players.end())
            continue;

        struct streets StreetInfo;
        street->CurentStreetInfo(&StreetInfo, UCID);

        int X = packet->Info[i].X/65536;
        int Y = packet->Info[i].Y/65536;

        int S = (int)packet->Info[i].Speed * 360 / 32768;

        int LastX = players.at(UCID).Info.X/65536;
        int LastY = players.at(UCID).Info.Y/65536;

        if (LastX==0 && LastY==0)
        {
            LastX=X;
            LastY=Y;
            players.at(UCID).Zone = 1;
        }
        float Dist = dl->Distance(X, Y, LastX, LastY);

        if (Dist<50 && S > 20)
        {
            players.at(UCID).Distance += Dist;
            /** Bonus **/
            bonuses[players.at(UCID).UName]["dist"] = bonuses[players.at(UCID).UName]["dist"].asFloat() + Dist;
        }

        if ( S > 30 && dl->GetLVL(UCID) < 20 )
        {
            if (S <= StreetInfo.SpeedLimit)
            {
                if ( dl->Islocked( UCID ) )
                    dl->Unlock( UCID );
                bank->AddCash(UCID, abs((int)Dist)/10, false);
                bank->RemFrBank(abs((int)Dist)/100);
            }
            else
            {
                if ( !( dl->Islocked( UCID ) ) )
                    dl->Lock( UCID );
            }
        }

        /** Bonus **/
        float Bonus_dist = bonuses[players.at(UCID).UName]["dist"].asFloat();
        float Bonus_count = bonuses[players.at(UCID).UName]["count"].asInt();
        if (Bonus_dist > BONUS_DISTANCE)
        {
            bonuses[players.at(UCID).UName]["dist"] = Bonus_dist - BONUS_DISTANCE;

            int bonus = 100+(50*(Bonus_count));
            bonuses[players.at(UCID).UName]["count"] = Bonus_count + 1;

            bank->AddCash(UCID, bonus, false);
            bank->RemFrBank( bonus );

            char bonus_c[64];
            sprintf(bonus_c, msg->_( UCID, "1500" ), bonus);
            insim->SendMTC(UCID, bonus_c);
        }

        /** Zones (PitSave, shop, etc) **/
        if (bank->InBank(UCID))
            players.at(UCID).Zone = 4;
        else if ( nrg->InCafe(UCID))
            players.at(UCID).Zone = 3;
        else if ( RCBaseClass::CheckPosition(TrackInf.PitCount, TrackInf.XPit, TrackInf.YPit, X, Y))
            players.at(UCID).Zone = 1;
        else if ( RCBaseClass::CheckPosition(TrackInf.ShopCount, TrackInf.XShop, TrackInf.YShop, X, Y))
        {
            if (players.at(UCID).Zone!=2)
            {
                players.at(UCID).Zone = 2;
                insim->SendMTC(UCID, msg->_( UCID, "ShopDialog1" ));
                insim->SendMTC(UCID, msg->_( UCID, "ShopDialog2" ));
                insim->SendMTC(UCID, msg->_( UCID, "ShopDialog3" ));
                //insim->SendMTC(UCID, msg->_( UCID, "ShopDialog4" ));
                //insim->SendMTC(UCID, msg->_( UCID, "ShopDialog5" ));
            }
        }
        else
            players.at(UCID).Zone = 0;

        if (players.at(UCID).debug == 1)
        {
            char TEST[64];
            sprintf(TEST, "X=%d Y=%d H=%d", X, Y, packet->Info[i].Heading/182);

            this->ButtonInfo(UCID, TEST);

        }

        /** Rent **/

        if(players.at(UCID).cars[players.at(UCID).CName].rent) {
            players.at(UCID).cars[players.at(UCID).CName].rentDistance += Dist;

            if(players.at(UCID).cars[players.at(UCID).CName].rentDistance > 1000) {
                players.at(UCID).cars[players.at(UCID).CName].rentDistance -= 1000;

                int rentPrice = cars[players.at(UCID).CName]["price"].asInt() / 2000; // 0.05% car price
                if (bank->GetCash(UCID) >= rentPrice) {
                    bank->RemCash(UCID, rentPrice);
                    bank->AddToBank( rentPrice );
                } else {
                    insim->SendMTC(UCID, "^C^1| ������������ ������� ��� ������ ������");
                }
            }

        }

        players.at(UCID).Info = packet->Info[i];
    }
}


void
RCCore::InsimMSO( struct IS_MSO* packet )
{
    byte UCID = packet->UCID;
    if (UCID == 0)
        return;

    if (packet->UserType == MSO_USER)
    {
        /** ������� **/
        int SwCount = msg->CheckSwear(packet->Msg);       // ����������� ��� ��, ������� ��� ������
        if (SwCount > 0)
        {
            int pay = SwCount * 2500;
            string str = "";
            if (SwCount > 1)
                str = " ^7(^3x" + ToString(SwCount) + "^7)";

            insim->SendMTC(UCID, StringFormat(msg->_(packet->UCID, "swear"), str.c_str(), pay));

            bank->RemCash(UCID, pay);
            bank->AddToBank(pay);
        }
        /** ######## **/

        /**  FLOOD ***/
        if (time(NULL) - players.at(UCID).FloodTime > 3)
            players.at(UCID).FloodCount = 1;

        if (players.at(UCID).FloodCount >= 5)
        {
            int pay = 1000;

            insim->SendMTC(UCID, StringFormat(msg->_(UCID, "flood"), pay));

            players.at(UCID).FloodCount = 0;
            bank->RemCash(UCID, pay);
            bank->AddToBank(pay);
        }

        players.at(UCID).FloodCount++;
        players.at(UCID).FloodTime = time(NULL);
        /** ###### **/
    }

    if (packet->UserType != MSO_PREFIX)
        return;

    string Username = xString(players.at(UCID).UName).toLower();

    xString Message = packet->Msg + packet->TextStart;
    vector<xString> args = Message.split(' ',1);

    //!help
    if (Message == "!info" || Message == "!^C����")
    {
        insim->SendBFN(UCID, 176, 239);
        InfoPage(UCID, 1);
        return;
    }

    if (Message == "!help" || Message == "!^C������")
    {
        help_cmds(UCID, 1);
        return;
    }

    if (Message == "!cars" || Message == "!^C������")
    {
        help_cmds(UCID, 2);
        return;
    }


    if (Message.find("!trans") == 0 || Message.find("!^C��������") == 0 )
    {
        DB_WHERE where;
        where.insert({"type","send"});
        where.insert({"message","%" + string(players.at(UCID).UName) + "%"});

        if (args.size() > 1 ){
            where.insert({"message", "%" + args[1].substr(0,24) + "%"});
        }

        DB_ROWS res = db->select({"date","message"},"log",where);

        if(res.size() > 0)
        {
            insim->SendMTC(UCID, msg->_(UCID, "TransfersH"));

            if(res.size() <= 20)
            {
                for(DB_ROW row: res)
                {
                    insim->SendMTC(UCID, "^1| ^C^7" + row["date"] + " " + row["message"]);
                }
            }
            else
            {
                for(int i = res.size()-19; i < res.size(); ++i)
                {
                    insim->SendMTC(UCID, "^1| ^C^7" + res[i]["date"] + " " + res[i]["message"]);
                }
            }
        }

        return;
    }

    //!show
    if (Message == "!show")
    {
        string Text;

        insim->SendMTC(255, "^1------------------------------");
        insim->SendMTC(255, players.at(UCID).PName, SND_SYSMESSAGE);
        insim->SendMTC(255, StringFormat(msg->_(UCID, "ShowCash"), bank->GetCash(UCID)));

#ifdef _RC_LEVEL_H

        float nextlvl = (pow(dl->GetLVL(UCID), 2)*0.5+100)*1000;
        int prog = int(dl->GetSkill(UCID)/nextlvl*100);

        insim->SendMTC(255, StringFormat(msg->_(UCID, "ShowLvl"), dl->GetLVL(UCID), prog));
#endif

        for (auto cr : players.at(UCID).cars)
        {

                string car = xString(cr.first).toLower();

                if(!cars.isMember(cr.first))
                    continue;

                if(!cars[cr.first]["name"].isString()){
                    cout << "bad carname for " << cr.first << " in show command" << endl;
                    continue;
                }

                if(players.at(UCID).cars[cr.first].rent)
                    continue;
/** disable tun
                char Tun[30];

                if (Items::Get()->getItemsByOwner(Username,"ECU", car).size() > 0)
                    strcpy(Tun, "^2E");
                else
                    strcpy(Tun, "^1E");

                if (Items::Get()->getItemsByOwner(Username,"TRB", car).size() > 0)
                    strcat(Tun, " ^2T");
                else
                    strcat(Tun, " ^1T");

                if (Items::Get()->getItemsByOwner(Username,"WHT", car).size() > 0)
                    strcat(Tun, " ^2W");
                else
                    strcat(Tun, " ^1W");
**/
                insim->SendMTC(255, StringFormat(msg->_(UCID, "ShowCars"), cars[cr.first]["name"].asCString(), cr.second.dist/1000));

        }

        insim->SendMTC(255, "^1------------------------------");
        return;
    }




    //!buy
    if (Message.find("!buy")==0 && players.at(packet->UCID).Zone == 2)
    {
        if ( args.size() < 2)
        {
            insim->SendMTC(packet->UCID, msg->_(packet->UCID, "SHOP_BUY_USAGE"));
            return;
        }



        string CarName = args[1].toUpper();

        if (!cars.isMember(CarName))
        {
            insim->SendMTC(packet->UCID, msg->_(packet->UCID, "SHOP_BUY_WE_HAVENT"));
            return;
        }

#ifdef _RC_LEVEL_H
        int needlvl = cars[CarName]["level"].asInt();
        if (dl->GetLVL(packet->UCID) < needlvl)
        {;
            insim->SendMTC(packet->UCID, StringFormat( msg->_(packet->UCID, "NEED_LVL"), needlvl));
            return;
        }
#endif

        if (bank->GetCash(packet->UCID) < cars[CarName]["price"].asInt())
        {
            char message[64];
            sprintf(message, msg->_(packet->UCID, "SHOP_BUY_NEED_MONEY"), cars[CarName]["price"].asInt());
            insim->SendMTC(packet->UCID, message);
            return;
        }



        if (players.at(packet->UCID).cars.find(CarName) != players.at(packet->UCID).cars.end() && players.at(packet->UCID).cars[CarName].rent == false)
        {
            insim->SendMTC(packet->UCID, msg->_(packet->UCID, "SHOP_BUY_ALREADY_HAVE"));
            return;
        }


        players.at(packet->UCID).cars[CarName].tuning=0;
        players.at(packet->UCID).cars[CarName].dist=0;

        if(players.at(packet->UCID).cars[CarName].rent == true){
            players.at(packet->UCID).cars[CarName].rent=false;
        } else {
            players.at(packet->UCID).PLC += cars[CarName]["plc"].asInt();
            insim->SendPLC(packet->UCID, players.at(packet->UCID).PLC);
        }

        bank->RemCash(packet->UCID, cars[CarName]["price"].asInt());
        bank->AddToBank(cars[CarName]["price"].asInt());

        CLog::log("RCCore", "shop",StringFormat("%s buy car %s", players.at(packet->UCID).UName.c_str(), CarName.c_str()));

        string sql;
        sql = StringFormat("INSERT INTO garage (username, car) VALUES ('%s', '%s');", players.at(packet->UCID).UName.c_str(), CarName.c_str() );
        db->exec(sql);

        string message;
        message = StringFormat(msg->_(packet->UCID, "SHOP_BUY_SUCCESS"), cars[CarName]["name"].asCString(), cars[CarName]["price"].asInt());
        insim->SendMTC(packet->UCID, message);


    }

    if (Message.find("!sell") == 0)
    {
        if (players.at(packet->UCID).Zone != 2)
        {
            insim->SendMTC(packet->UCID, msg->_( packet->UCID, "2000" ));
            return;
        }

        if ( args.size() < 2)
        {
            insim->SendMTC(packet->UCID, msg->_( packet->UCID, "SHOP_SELL_USAGE"));
            return;
        }

        string CarName = args[1].toUpper();

        if (CarName == "UF1")
            return;

        if (!cars.isMember(CarName))
        {
            insim->SendMTC(packet->UCID, msg->_( packet->UCID, "SHOP_BUY_WE_HAVENT"));
            return;
        }

        // if user now  on this car
        if (CarName == players.at(packet->UCID).CName)
        {
            insim->SendMTC(packet->UCID, msg->_( packet->UCID, "1009" ));
            return;
        }

        if(players.at(packet->UCID).cars.find(CarName) == players.at(packet->UCID).cars.end()) {
            insim->SendMTC(packet->UCID, msg->_( packet->UCID, "SHOP_SELL_YOU_HAVENT"));
            return;
        }

        if(players.at(packet->UCID).cars[CarName].rent) {
            insim->SendMTC(packet->UCID, msg->_( packet->UCID, "SHOP_SELL_RENTED"));
            return;
        }

        players.at(packet->UCID).cars.erase(CarName);

        int sellPrice = cars[CarName]["price"].asInt()*7/10;

        CLog::log("RCCore", "shop",StringFormat("%s sell car %s", players.at(packet->UCID).UName.c_str(), CarName));

        bank->AddCash(packet->UCID, sellPrice, true);
        bank->RemFrBank(sellPrice);

        players.at(packet->UCID).PLC -= cars[CarName]["plc"].asInt();
        insim->SendPLC(packet->UCID, players.at(packet->UCID).PLC);

        string sql;
        sql = StringFormat("DELETE FROM garage WHERE  username = '%s' AND  car = '%s'", players.at(packet->UCID).UName.c_str(), CarName.c_str() );
        db->exec(sql);


        insim->SendMTC(packet->UCID, StringFormat(msg->_( packet->UCID, "SHOP_SELL_SUCCESS"), CarName.c_str(), sellPrice));
    }

    if (args[0] == "!rent")
    {
        if (args.size() != 2) {
            insim->SendMTC(packet->UCID, msg->_( packet->UCID, "SHOP_RENT_USAGE"));
            return;
        }

        xString CarName = args[1].toUpper();

        if (!cars.isMember(CarName))
        {
            insim->SendMTC(packet->UCID, msg->_( packet->UCID, "SHOP_BUY_WE_HAVENT"));
            return;
        }

        if(players.at(packet->UCID).cars.find(CarName) != players.at(packet->UCID).cars.end()) {
            insim->SendMTC(packet->UCID, msg->_( packet->UCID, "SHOP_BUY_ALREADY_HAVE"));
            return;
        }

        #ifdef _RC_LEVEL_H
        int needlvl = cars[CarName]["level"].asInt();

        if (dl->GetLVL(packet->UCID) < needlvl)
        {
            insim->SendMTC(packet->UCID, StringFormat( msg->_(packet->UCID, "NEED_LVL"), needlvl));
            return;
        }
        #endif

        int rentPrice = cars[CarName]["price"].asInt() / 2000; // 0.05% car price

        if (bank->GetCash(UCID) < rentPrice) {

            insim->SendMTC(packet->UCID, StringFormat(msg->_( packet->UCID, "SHOP_BUY_NEED_MONEY"), rentPrice));
            return;
        }

        players.at(packet->UCID).cars[CarName].tuning=0;
        players.at(packet->UCID).cars[CarName].dist=0;
        players.at(packet->UCID).cars[CarName].rent=1;
        players.at(packet->UCID).cars[CarName].rentDistance=0;

        players.at(packet->UCID).PLC += cars[CarName]["plc"].asInt();
        insim->SendPLC(packet->UCID, players.at(packet->UCID).PLC);

        insim->SendMTC(packet->UCID, StringFormat(msg->_( packet->UCID, "SHOP_RENT_SUCCESS"), cars[CarName]["name"].asCString(), rentPrice));

    }

    if (Message == "!reload" && (players.at(UCID).Admin || UCID == 0) )
    {
        RCBaseClass::CCText("^7Config reload");

        insim->SendTiny(TINY_RST, 255);

        for (auto& plr: players)
            if(plr.second.Admin)
                insim->SendMTC(plr.first, msg->_( packet->UCID, "CONFIG_RELOADED"));
        return;
    }

    if (Message == "!debug")
    {
        db->debug = !db->debug;

        if (players.at(UCID).debug == 0)
            players.at(UCID).debug = 1;
        else
        {

            this->ClearButtonInfo(UCID);

            players.at(UCID).debug = 0;
        }

        return;
    }
    //!users
    if (Message == "!users" || Message == "!^C�����")
    {
        ShowUsersList(UCID);
        return;
    }

    /**
    * Options
    * a get method always return string
    * !options get module varName
    *
    * a set method always need 4 arguments
    * !options set string|int moduleName varName value
    *
    * a del method just delete parameter
    */

    if(Message == "!options" && players.at(UCID).Admin)
    {
        insim->SendMTC(UCID,"^3!options get module variable");
        insim->SendMTC(UCID,"^3!options del module variable");
        insim->SendMTC(UCID,"^3!options set string|int|double module variable value");

        vector<string> modules = COptions::getModules();
        if(modules.size() > 0)
        {
            insim->SendMTC(UCID,"^3Available modules:");
            for(auto m: modules)
            {
                insim->SendMTC(UCID,"^3" + m);
            }
        }
    }

    if(args.size() == 2 && args[0] == "!options"  && players.at(UCID).Admin)
    {
        vector<string> modules = COptions::getModules();
        for(auto m: modules)
        {
            if(m == args[1])
            {
                vector<string> params = COptions::getParams(m);
                if(params.size() > 0)
                {
                    insim->SendMTC(UCID,"^3Available params for module "+m+":");
                    for(auto p: params)
                    {
                        string type = COptions::getType(m, p);
                        if(type == "string")
                            insim->SendMTC(UCID,"^3" + m + "->" + p + " = " + COptions::getString(m, p) + "(" + type + ")");
                        else if(type == "int")
                            insim->SendMTC(UCID,"^3" + m + "->" + p + " = " + ToString(COptions::getInt(m, p)) + "(" + type + ")");
                        else if(type == "double")
                            insim->SendMTC(UCID,"^3" + m + "->" + p + " = " + ToString(COptions::getDouble(m, p)) + "(" + type + ")");
                    }
                }
                break;
            }
        }
    }

    if((args.size() == 4 || args.size() == 6) && args[0] == "!options"  && players.at(UCID).Admin)
    {
        if(args[1] == "get" && args.size() == 4)
        {
            if(COptions::isMember(args[2], args[3]))
            {
                string m = args[2];
                string p = args[3];
                string type = COptions::getType(m, p);

                if(type == "string")
                    insim->SendMTC(UCID,"^3" + m + "->" + p + " = " + COptions::getString(m, p) + "(" + type + ")");
                else if(type == "int")
                    insim->SendMTC(UCID,"^3" + m + "->" + p + " = " + ToString(COptions::getInt(m, p)) + "(" + type + ")");
                else if(type == "double")
                    insim->SendMTC(UCID,"^3" + m + "->" + p + " = " + ToString(COptions::getDouble(m, p)) + "(" + type + ")");
            }
            else
            {
                insim->SendMTC(
                    UCID,
                    "^3COptions: ^1don't have "+args[2]+" || "+args[2]+"["+args[3]+"]"
                );
            }

        }
        else if(args[1] == "set" && args.size() == 6)
        {
            bool setted = false;
            if(args[2] == "string")
            {
                setted = COptions::Set(args[3], args[4], args[5]);
            }
            else if(args[2] == "int")
            {
                setted = COptions::Set(args[3], args[4], args[5].asInt());
            }
            else if(args[2] == "double")
            {
                setted = COptions::Set(args[3], args[4], args[5].asDouble());
            }

            if(setted)
            {
                insim->SendMTC(
                    UCID,
                    "^3COptions: ^2set "+args[3]+"["+args[4]+"] to " + args[5]
                );
            }
        }
        else if(args[1] == "del" && args.size() == 4)
        {
            if(COptions::Delete(args[2], args[3]))
            {
               insim->SendMTC(
                    UCID,
                    "^3COptions: "+args[3]+"["+args[4]+"] deleted"
                );
            }
        }
    }

    if(Message == "!km")
    {
        players[UCID].ShowKM = !players[UCID].ShowKM;
    }
}

void
RCCore::InsimNCN( struct IS_NCN* packet )
{
    if (packet->UCID == 0)
        return;

    if (packet->ReqI == 0)
        RCBaseClass::CCText("^6>> connected " + (string)packet->UName + ", UCID: " + ToString(packet->UCID));


    players[packet->UCID].UName = packet->UName;
    players[packet->UCID].PName = packet->PName;
    players[packet->UCID].Admin = packet->Admin;

    if(!packet->Admin)
    {
        players[packet->UCID].Admin = msg->isAdmin(packet->UName);
    }

    players[packet->UCID].Zone = 1;

    read_user_cars(packet->UCID);


    insim->SendMTC(packet->UCID, msg->_( packet->UCID, "Help1" ));
    insim->SendMTC(packet->UCID, msg->_( packet->UCID, "^1| ^7LFS Forum: ^3https://www.lfs.net/forum/post/1921776" ));
    insim->SendMTC(packet->UCID, msg->_( packet->UCID, "^1| ^7Discord: ^3https://discord.gg/tHKteJZWVy" ));
    insim->SendMTC(packet->UCID, msg->_( packet->UCID, "^1| ^7VK: ^3https://vk.com/russiancruise" ));

    insim->SendMTC(packet->UCID, msg->_( packet->UCID, "Help14" ));

    if(bonuses.isMember(packet->UName) && bonuses[packet->UName].isMember("time"))
   {
        u_int t = bonuses[packet->UName]["time"].asUInt();

        if(t+BONUS_TIMEOUT < time(NULL))
            bonuses[packet->UName] = Json::nullValue;
   }
}

void
RCCore::InsimNPL( struct IS_NPL* packet )
{
    players.at(packet->UCID).Zone = 1;
    players[packet->UCID].PenReason = 0;

    xString carName = expandCar(packet->CName);

    // �������� ������� ������ �� ������� � ������ ������

    if (players.at(packet->UCID).cars.find(carName) == players.at(packet->UCID).cars.end())
    {
        insim->SendMST("/spec " + string(players.at(packet->UCID).UName));
        insim->SendMTC(packet->UCID, msg->_( packet->UCID, "2404" ));
        help_cmds(packet->UCID, 2);
        insim->SendJRR(JRR_REJECT,packet->UCID);
        return;
    }

    if(RCCore::isf_flag&ISF_REQ_JOIN && packet->NumP != 0 && packet->ReqI != 255)
        return;

    if (packet->PType == 6)
        return;

    insim->SendBFNAll( packet->UCID );

    players.at(packet->UCID).CName = carName;
    players.at(packet->UCID).Distance = players.at(packet->UCID).cars[carName].dist;

#ifdef _RC_LEVEL_H
    int needlvl = cars[carName]["level"].asInt();

    if (dl->GetLVL(packet->UCID) < needlvl)
    {
        insim->SendMTC(packet->UCID, StringFormat( msg->_(packet->UCID, "NEED_LVL"), needlvl));
        insim->SendMTC(packet->UCID, msg->_( packet->UCID, "2404" ));

        char Text2[64];
        strcpy(Text2, "^1| ^2");
        for (auto car: players.at(packet->UCID).cars)
        {
            if ( cars[car.first]["level"].asInt() <= dl->GetLVL(packet->UCID))
            {
                strcat(Text2, car.first.c_str());
                strcat(Text2, " ");
            }
        }
        insim->SendMTC(packet->UCID, Text2);
        insim->SendJRR(JRR_REJECT,packet->UCID);
        return;
    }
#endif

#ifdef _RC_POLICE_H
    if (police->isArested(packet->UCID))
    {
        insim->SendJRR(JRR_REJECT,packet->UCID);
        return;
    }
#endif

#ifdef _RC_ENERGY_H
    if (nrg->GetEnergy(packet->UCID) < 2)
    {
        insim->SendJRR(JRR_REJECT, packet->UCID);

        insim->SendMTC(packet->UCID, msg->_(packet->UCID, "2402"));
        insim->SendMTC(packet->UCID, msg->_(packet->UCID, "2403"));

        return;
    }
#endif

    insim->SendJRR(JRR_SPAWN,packet->UCID);
    PLIDtoUCID[packet->PLID] = packet->UCID;
}


void
RCCore::InsimPEN( struct IS_PEN* packet )
{
    byte UCID = PLIDtoUCID[packet->PLID];

#ifdef _RC_TAXI_H
    if (packet->Reason == PENR_WRONG_WAY)
        taxi->PassDead(UCID);
#endif

    players.at(UCID).PenReason = packet->Reason;
}

void
RCCore::InsimPLL( struct IS_PLL* packet )
{
    byte UCID = PLIDtoUCID[packet->PLID];

    if(players.find(UCID) != players.end())
    {
        memset(&players.at(UCID).Info, 0, sizeof(CompCar));
    }
    save_car(UCID);

#ifdef _RC_POLICE_H
    if ( police->InPursuite( UCID ) == 1 )
    {
        int pay = 10000;

        char str[96];
        sprintf(str, msg->_(UCID, "2600"), pay);
        insim->SendMTC(UCID, str);

        bank->RemCash(UCID, pay);
        bank->AddToBank(pay);

        if ( dl->GetSkill( UCID ) > 10 )
            dl->RemSkill( UCID, 10);
    }
    else

#endif

    if (players.at(UCID).Zone != 1 && nrg->GetEnergy(UCID) > 3 && players.at(UCID).PenReason != PENR_WRONG_WAY && !police->isArested(UCID))
    {
        insim->SendMTC(UCID, msg->_(UCID,"2602"));

        bonuses[players.at(UCID).UName]["backup"]["count"] = bonuses[players.at(UCID).UName]["count"];
        bonuses[players.at(UCID).UName]["backup"]["dist"] = bonuses[players.at(UCID).UName]["dist"];

        bonuses[players.at(UCID).UName]["count"] = 0;
        bonuses[players.at(UCID).UName]["dist"] = 0;
    }


    players.at(UCID).PLL_time = time(NULL);
    players.at(UCID).Zone = 1;              // ����� ��� ���?
    PLIDtoUCID.erase(packet->PLID);
}

void
RCCore::InsimPLP( struct IS_PLP* packet )
{
    // Find player and set his PLID to 0
    byte UCID = PLIDtoUCID[packet->PLID];

    memset(&players.at(UCID).Info, 0, sizeof(CompCar));

    save_car(UCID);

#ifdef _RC_POLICE_H
    if ( police->InPursuite( UCID ) == 1 )
    {
        int pay = 10000;

        char str[96];
        sprintf(str, msg->_(UCID, "2700"), pay);
        insim->SendMTC(UCID, str);

        bank->RemCash(UCID, pay);
        bank->AddToBank(pay);

        if ( dl->GetSkill( UCID ) > 10 )
            dl->RemSkill(UCID, 10);
    }
    else

#endif

    if (players.at(UCID).Zone != 1 && nrg->GetEnergy(UCID) > 10 && players.at(UCID).PenReason != PENR_WRONG_WAY)
    {
        insim->SendMTC(UCID, msg->_(UCID,"2702"));

        bonuses[players.at(UCID).UName]["count"] = 0;
        bonuses[players.at(UCID).UName]["dist"] = 0;
    }

    players.at(UCID).Zone = 1;
    PLIDtoUCID.erase(packet->PLID);
}

void
RCCore::ReadConfig(const char* Track)
{
    this->Track = Track;

    read_car();

    read_track();

    CCText("  ^7RCCore\t\t^2OK");
};

void
RCCore::InsimVTN( struct IS_VTN* packet )
{
    if (packet->UCID == 0)
        return;


    insim->SendMST("/cv");
}

void
RCCore::read_track()
{

    string file = StringFormat("%s/RCCore/tracks/%s.txt" , RootDir , Track.c_str());

    ifstream readf (file, ios::in);

    if(!readf.is_open())
    {
        cout << "Can't find " << file << endl;
        return;
    }

    memset(&TrackInf, 0, sizeof(track_info));

    while (readf.good())
    {
        char str[128];
        readf.getline(str, 128);

        if (strstr(str, "//")) {}
        if (strlen(str) > 1)
        {
            if (strncmp(str, "/pitzone", 8)==0)
            {
                readf.getline(str, 128);
                int count = atoi(str);

                TrackInf.PitCount = count;

                if ( TrackInf.XPit != NULL )
                    delete[] TrackInf.XPit;

                if ( TrackInf.YPit != NULL )
                    delete[] TrackInf.YPit;

                TrackInf.XPit = new int[count];
                TrackInf.YPit = new int[count];

                for (int i=0 ; i<count; i++)
                {
                    readf.getline(str, 128);
                    char * X;
                    char * Y;
                    X = strtok (str, ";");
                    Y = strtok (NULL, ";");
                    TrackInf.XPit[i] = atoi(X);
                    TrackInf.YPit[i] = atoi(Y);
                }
            }

            if (strncmp(str, "/shop", 5)==0)
            {
                readf.getline(str, 128);
                int count = atoi(str);
                TrackInf.ShopCount = count;

                if ( TrackInf.XShop != NULL )
                    delete[] TrackInf.XShop;

                if ( TrackInf.YShop != NULL )
                    delete[] TrackInf.YShop;

                TrackInf.XShop = new int[count];
                TrackInf.YShop = new int[count];

                for (int i=0 ; i<count; i++)
                {
                    readf.getline(str, 128);
                    char * X;
                    char * Y;
                    X = strtok (str, ";");
                    Y = strtok (NULL, ";");
                    TrackInf.XShop[i] = atoi(X);
                    TrackInf.YShop[i] = atoi(Y);
                }
            }
        } // if strlen > 0
    } //while readf.good()

    readf.close();

}

void
RCCore::read_car()
{
    string file = StringFormat("%s/cars.json" , RootDir);

    fstream readf (file, ios::in);

    if(!readf.is_open())
    {
        cout <<"RCCore ERROR: file " << file << " not found" << endl;
        return ;
    }

    bool readed = configReader.parse( readf, cars, false );

    readf.close();

    if ( !readed )
    {
        // report to the user the failure and their locations in the document.
        cout  << "Failed to parse configuration\n"
              << configReader.getFormattedErrorMessages();
        return;
    }
}


void
RCCore::Event ()
{
    // �������
    for (auto& plr: players)
    {
        if (plr.first == 0 || plr.first == 255)
            continue;

        string str;
        switch(plr.second.Zone){
            case 1:
                str = msg->_(plr.first, "PitSaveGood");
                break;

            case 2:
                str = msg->_( plr.first, "401" );
                break;

            case 3:
                str = msg->_( plr.first, "402" );
                break;

            case 4:
                str = msg->_( plr.first, "404" );
                break;

            default:
                str = msg->_(plr.first, "PitSaveNotGood");
        }

        insim->SendButton(255, plr.first, 3, 88, 1, 12, 8, 32+7, str);
        /** Bonus **/
        int cash = 0;
        if(bonuses[plr.second.UName]["count"].asInt() != 0)
            cash = 100 + 50*(bonuses[plr.second.UName]["count"].asInt() - 1);

        float percent = bonuses[plr.second.UName]["dist"].asFloat() / BONUS_DISTANCE * 100;

        if(plr.second.ShowKM)
            insim->SendButton(255, plr.first, 4, 70, 5, 18, 4, 32, StringFormat(msg->_( plr.first, "Dist" ), (plr.second.Distance / 1000)));
        else
            insim->SendButton(255, plr.first, 4, 70, 5, 18, 4, 32, StringFormat(msg->_( plr.first, "bonus_button" ), cash, percent));
    }
}


void
RCCore::ShowUsersList(byte UCID)
{
    insim->SendBFN(UCID, 21, 80);

    byte count = 0, L = 0, T = 0;
    for (auto& plr: players)
    {
        // Viva La �������
        if(plr.first == 0 || plr.second.PName.empty())
            continue;

        if (count == 24)
        {
            L += 22;
            T = 0;
        }

        insim->SendButton(plr.first, UCID, 21+count++, 1 + L, 191 - 4*T++, 22, 4, 16 + 8, players.at(plr.first).PName);
    }
    insim->SendButton(255, UCID, 69, 1, 195, count > 24 ? 44 : 22, 4, 16 + 8, msg->_( UCID, "604" ));
}

void
RCCore::InfoPage(byte UCID, byte b_type)
{
    list<string> about_text = {
        StringFormat("^7RUSSIAN CRUISE  %s", RCCore::IS_PRODUCT_NAME.c_str()),
        StringFormat("^2Build date:  %s.%s.%s", AutoVersion::RC_YEAR, AutoVersion::RC_MONTH, AutoVersion::RC_DATE),
        "^C^7Developers: denis-takumi, Lexanom",
        "^C^7Telegram: @turbosnail",
        "^7",
        "^7",
        "^7More information",
        "^7http://vk.com/russiancruise",
        "^7",
        "^7Thanks: ^3repeat83, Vladimir_nose"
    };

    list<string> help_text = {
        StringFormat("^7RUSSIAN CRUISE  %s", RCCore::IS_PRODUCT_NAME.c_str()),
        msg->_(UCID,"info_help_1"),
        msg->_(UCID,"info_help_2"),
        msg->_(UCID,"info_help_3"),
        msg->_(UCID,"info_help_4"),
        msg->_(UCID,"info_help_5"),
        msg->_(UCID,"info_help_6"),
        msg->_(UCID,"info_help_7")
    };

    byte c;
    if (b_type == 1) c = cars.size() / 2;         //���������� ����� ��� 1 �������
    #ifdef _RC_POLICE_H
    if (b_type == 2) c = police->GetFineCount();  //���������� ����� ��� 2 �������
    #endif
    if (b_type == 3) c = about_text.size();   //��, ��, �� ������
    if (b_type == 4) c = help_text.size();   //��, ��, �� ������
    byte
    id=183,             //��������� �� ������
    l=100, t=90,        //����� ����
    hButton=4,          //������ ����� ������
    w=100,              //������ ����
    h=16+c*hButton;     //������ ����

    insim->SendButton(255, UCID, 176, l-w/2, t-h/2, w, h+8, 32, "");                                   //���
    id++;
    insim->SendButton(255, UCID, 177, l-w/2, t-h/2, w, h+8, 32, "");
    id++;
    insim->SendButton(254, UCID, 178, l-7, t-h/2+h+1, 14, 6, 16+8, "^2OK");                            //����������
    id++;
    insim->SendButton(255, UCID, id++, l-w/2, t-h/2, 25, 10, 3+64, "RUSSIAN CRUISE");                  //���������
    insim->SendButton(255, UCID, id++, l-w/2+24, t-h/2+2, 20, 3, 5+64, RCCore::IS_PRODUCT_NAME);               //������

    insim->SendButton(255, UCID, 180, l-w/2+1, t-h/2+9, 16, 6, 16+8, msg->_(UCID, "200"));  //������� ���
    id++;
    insim->SendButton(255, UCID, 181, l-w/2+17, t-h/2+9, 16, 6, 16+8, msg->_(UCID, "201")); //���
    id++;
    insim->SendButton(255, UCID, 182, l-w/2+33, t-h/2+9, 16, 6, 16+8, msg->_(UCID, "202")); //���������
    id++;
    //insim->SendButton(255, UCID, 183, l-w/2+49, t-h/2+9, 16, 6, 16+8, msg->_(UCID, "help_tab")); //���������
    id++;

    map<byte, string>tabs = {
        pair<byte, string>{131, msg->_(UCID, "200")},
        pair<byte, string>{132, msg->_(UCID, "201")},
        pair<byte, string>{133, msg->_(UCID, "202")}
    };

    if (b_type == 1)
    {
        int it = 1;
        int delimetr = cars.size() / 2;

        for(auto cit = cars.begin(); cit != cars.end(); ++cit)
        {
            Json::Value car = cars[cit.memberName()]; // cit.name() return car code

            if(!car["name"].isString())
            {
                cout << "Bad car name for " << cit.memberName() << "in InfoPage "<< endl;
                continue;
            }

            int plW = 0,
                plH = 1;

            if (it > delimetr)
            {
                plW = w / 2 - 1;
                plH = delimetr+1;
            }

            int sellPrice = car["price"].asInt() * 7 / 10;

            string str = StringFormat("^2%s ^3(%s) ^2%d RUB ^3lvl: %d", car["name"].asCString(), cit.memberName(), car["price"].asInt(),  car["level"].asInt());

            if (dl->GetLVL(UCID) < car["level"].asInt())
                str = StripText(str);

            insim->SendButton(255, UCID, id++, l-w/2+1+plW, t-h/2+16+hButton*(it-plH), w/2-1, hButton, 16+64, str);
            it++;
        }
    }
#ifdef _RC_POLICE_H
    else if (b_type == 2)
    {
        int i = 0;
        for(xString fineID: police->jFines.getMemberNames())
        {
            string fineName = police->GetFineName(UCID,fineID.asInt());
            string text = StringFormat("^7ID = %s. %s (^2%d %s^7)",fineID.c_str(), fineName.c_str(), police->jFines[fineID]["cash"].asInt(), msg->_(UCID,"currency"));

            insim->SendButton(255, UCID, id++, l-w/2+1, t-h/2+16+hButton * i++, w-2, hButton, 16+64, text);
        }
    }
#endif

    else if (b_type == 3)
    {
        byte i = 0;
        for (auto& txt: about_text)
        {
            insim->SendButton(255, UCID, id++, l-w/2+1, t-h/2+16+hButton*i++, w-2, hButton, 0, txt);
        }

    }

    else if (b_type == 4)
    {
        byte i = 0;
        for (auto& txt: help_text)
        {
            insim->SendButton(255, UCID, id++, l-w/2+1, t-h/2+16+hButton*i++, w-2, hButton, 0, txt);
        }

    }


    insim->SendBFN(UCID, id, 239);
}

void
RCCore::help_cmds (byte UCID, byte h_type)
{
    if (h_type == 1) // commands
    {
        insim->SendMTC(UCID, msg->_(UCID, "Help1"));
        insim->SendMTC(UCID, StringFormat(msg->_(UCID, "Help2"), msg->GetLangList().c_str()));
        insim->SendMTC(UCID, msg->_(UCID, "Help3"));
        insim->SendMTC(UCID, msg->_(UCID, "Help4"));
        insim->SendMTC(UCID, msg->_(UCID, "Help5"));
        insim->SendMTC(UCID, msg->_(UCID, "Help6"));
        insim->SendMTC(UCID, msg->_(UCID, "Help7"));
        insim->SendMTC(UCID, msg->_(UCID, "Help8"));
        insim->SendMTC(UCID, msg->_(UCID, "Help9"));
        insim->SendMTC(UCID, msg->_(UCID, "Help10"));
        insim->SendMTC(UCID, msg->_(UCID, "Help11"));
        insim->SendMTC(UCID, msg->_(UCID, "Help12"));
        insim->SendMTC(UCID, msg->_(UCID, "Help13"));
        insim->SendMTC(UCID, msg->_(UCID, "Help15"));
    }
    if (h_type == 2) // cars
    {
        insim->SendMTC(UCID, msg->_( UCID, "3100" ));
        for ( auto i : players.at(UCID).cars)
        {
            string Username = xString(players.at(UCID).UName).toLower();
            string car = xString(i.first).toLower();

            if(!cars.isMember(i.first))
                continue;

            if(players.at(UCID).cars[i.first].rent)
                continue;

            if(!cars[i.first]["name"].isString()){
                cout << "Bad car name for " << car << "in help_cmds "<< endl;
                continue;
            }
/** disable tun
            char Tun[30];

            if (Items::Get()->getItemsByOwner(Username,"ECU", car).size() > 0)
            {
                strcpy( Tun, "^2E");
            }
            else
            {
                strcpy( Tun, "^1E");
            }

            if (Items::Get()->getItemsByOwner(Username,"TRB", car).size() > 0)
            {
                strcat( Tun, " ^2T");
            }
            else
            {
                strcat( Tun, " ^1T");
            }

            if (Items::Get()->getItemsByOwner(Username,"WHT", car).size() > 0)
            {
                strcat( Tun, " ^2W");
            }
            else
            {
                strcat( Tun, " ^1W");
            }
**/
            insim->SendMTC(UCID, StringFormat("^1| " + (string)msg->_(UCID, "ShowCars"), cars[i.first]["name"].asCString(), i.second.dist/1000));

        }
    }
}

void
RCCore::read_user_cars(byte UCID)
{
    DB_ROWS result = db->select({"car","tuning","dist"},"garage",{{"username",players.at(UCID).UName}});

    if ( result.size() > 0 )
    {
        int i = 0;
        for ( auto row: result )
        {
            if(!cars.isMember(row["car"])) // dont load car if it disabled on server
                continue;

            players.at(UCID).cars[row["car"]].tuning = row["tuning"].asInt();
            players.at(UCID).cars[row["car"]].dist = row["dist"].asFloat();
            players.at(UCID).cars[row["car"]].rent = false;
            /** map<> **/

            players.at(UCID).PLC += cars[row["car"]]["plc"].asInt();

            i++;
        }
    }
    else
    {
        // add user first car. replace UF1 to D19B9B (SEAZ 11113 OKA)
        string carCode = COptions::getString(ClassName, "DEFAULT_CAR_CODE");

        if (!db->insert("garage",{{"car", carCode},{"username",string(players.at(UCID).UName)}}))
            RCBaseClass::CCText("^3RCMain: ^7new user " + (string)players.at(UCID).UName);


        players.at(UCID).cars[carCode].tuning = 0;
        players.at(UCID).cars[carCode].dist = 0;

        players.at(UCID).PLC += cars[carCode]["plc"].asInt();

        save_user_cars(UCID);
        //InfoPage(UCID,4);
    }

    insim->SendPLC(UCID, players.at(UCID).PLC);
}

void
RCCore::save_user_cars (byte UCID)
{
    for (auto i : players.at(UCID).cars)
    {

        if (i.second.rent)
            continue;

        string query = StringFormat("UPDATE garage SET tuning = %d, dist = %f WHERE car = '%s' AND username = '%s';",
            i.second.tuning,
            i.second.dist,
            i.first.c_str(),
            players.at(UCID).UName.c_str());

        if (!db->exec(query))
            printf("Bank Error: MySQL Query Save\n");

    }
}

void
RCCore::save_car(byte UCID)
{
    players.at(UCID).cars[players.at(UCID).CName].dist = (int)players.at(UCID).Distance;
}

void
RCCore::Save(byte UCID)
{
    if(UCID == 255)
        return;

    save_car(UCID);
    save_user_cars(UCID);

    insim->SendMTC(UCID, msg->_(UCID,"3000"));
}

bool
RCCore::isAdmin(byte UCID)
{
    return players.at(UCID).Admin;
}

void
RCCore::ReadBonuses()
{
    string filename = string(RootDir) + "/bonuses.json";

    if(!RCBaseClass::FileExists(filename))
    {
        RCBaseClass::CCText("^1Bonus not found");
        return;
    }

    ifstream file;

    file.open(filename, ios::binary);

    if( !file.is_open() )
    {
        RCBaseClass::CCText("^1Failed to open bonuses file");
        return;
    }

    bool readed = bonusesReader.parse( file, bonuses, false );

    if ( !readed )
    {
        cout << "Failed to parse bonuses\n" << bonusesReader.getFormattedErrorMessages();
    }

    file.close();

    RCBaseClass::CCText("^2Bonus restored");
    return;
}

void
RCCore::SaveBonuses()
{
    string filename = string(RootDir) + "/bonuses.json";

    ofstream f;
    f.open(filename, ios::out);
    f << bonusesWriter.write( bonuses );
    f.close();
    RCBaseClass::CCText("^2Bonus saved");
}

bool
RCCore::CanSave(byte UCID)
{
    time_t now;
    time(&now);

    if ((now - players.at(UCID).LastSave) < 5*3600)
    {
        insim->SendMTC(UCID, msg->_(UCID, "SAVE_MANY_TIME"));
        return false;
    }
    players.at(UCID).LastSave = now;
    return true;
}

void
RCCore::SaveAll()
{
    for( auto i: players)
    {
        Save(i.first);
    }
}

bool
RCCore::inShop(byte UCID)
{
    return players.at(UCID).Zone == 2;
}
