using namespace std;

#include "RCDrivingLicense.h"

RCDL*
RCDL::self = nullptr;

RCDL*
RCDL::getInstance()
{
    if(!self)
        self = new RCDL();

    return self;
}

RCDL::RCDL()
{
    ClassName = "RCDL";
}

RCDL::~RCDL()
{

}

int RCDL::GetLVL(byte UCID)
{
    return players[ UCID ].LVL;
}

int RCDL::GetSkill(byte UCID)
{
    return players[ UCID ].Skill;
}

bool RCDL::AddSkill(byte UCID, float coef)
{
    if (coef <= 0)
        coef = 1;

    if ( !Islocked( UCID ) )
    {
        players[ UCID ].Skill += (250 * players[ UCID ].LVL ) * coef;

        char Text[64];
        float nextlvl = ( pow( players[ UCID ].LVL, 2 ) * 0.5 + 100 ) * 1000;
        float skl = ( players[ UCID ].LVL * 250 / nextlvl ) * 100 * coef;
        sprintf( Text, msg->_(UCID, "AddSkill"), skl);
        insim->SendMTC( UCID, Text );
        return true;
    }
    return false;
}

bool RCDL::RemSkill(byte UCID, float coef)
{
    if (coef < 0)
        return false;

    if ( !Islocked( UCID ) )
    {
        if ( players[ UCID ].Skill < 500 * players[ UCID ].LVL * coef )
        {

            if ( players[ UCID ].LVL > 0 )
            {
                players[ UCID ].Skill = ( ( pow( players[ UCID ].LVL - 1 , 2 ) * 0.5 + 100 ) * 1000 ) - ( 500 * players[ UCID ].LVL * coef - players[ UCID ].Skill );
                players[ UCID ].LVL --;
            }
            else
            {
                players[ UCID ].Skill = 0;
            }
        }
        else
        {
            players[ UCID ].Skill -= 500 * players[ UCID ].LVL * coef;
        }

        char Text[64];
        float nextlvl = ( pow( players[ UCID ].LVL , 2 ) * 0.5 + 100 ) * 1000;
        float skl = ( players[ UCID ].LVL * 500 / nextlvl ) * 100;
        sprintf(Text, msg->_(UCID, "RemSkill"), skl * coef );
        insim->SendMTC( UCID , Text);

        return true;
    }

    return false;
}

bool RCDL::Lock(byte UCID)
{
    players[ UCID ].Lock = 1;
    return true;
}

bool RCDL::Unlock(byte UCID)
{
    players[ UCID ].Lock = 0;
    return true;
}

bool RCDL::Islocked(byte UCID)
{
    if (players[ UCID ].Lock == 1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

int RCDL::init(const char* Dir)
{
    strcpy(RootDir,Dir);

    this->db = DBMySQL::Get();
    if (!this->db)
    {
        CCText("^3RCDL:\t\t^1Can't sctruct MySQL Connector");
        return -1;
    }

    insim = CInsim::getInstance();
    if (!insim)
    {
        CCText("^3RCDL:\t\t^1Can't struct CInsim class");
        return -1;
    }

    msg = RCMessage::getInstance();
    if (!msg)
    {
        CCText("^3RCDL:\t\t^1Can't struct RCMessage class");
        return -1;
    }

    inited = 1;

    CCText("^3"+ClassName+":\t\t^2inited");

    return 0;
}

void RCDL::InsimNCN( struct IS_NCN* packet )
{
    if (packet->UCID == 0)
    {
        return;
    }

    // Copy all the player data we need into the players[] array
    players[ packet->UCID ].UName = packet->UName;
    players[ packet->UCID ].PName = packet->PName;
    players[packet->UCID].Admin = packet->Admin;

    if(!packet->Admin)
    {
        players[packet->UCID].Admin = this->isAdmin(packet->UName);
    }

    DB_ROWS result = db->select({"lvl","skill"},"dl",{{"username",packet->UName}});
    DB_ROW row;

    if( result.size() != 0 )
    {
    	row = result.front();
        players[packet->UCID].LVL = row["lvl"].asInt();
        players[packet->UCID].Skill = row["skill"].asFloat();
    }
    else
    {
        CCText("^3" + ClassName + ": ^7new user " + packet->UName);

        db->exec(StringFormat("INSERT INTO dl (username) VALUES ('%s');", packet->UName));

        players[ packet->UCID ].LVL = 0;
        players[ packet->UCID ].Skill = 0;
        players[ packet->UCID ].Loaded = true;
        Save( packet->UCID );

    }
    players[ packet->UCID ].Loaded = true;

    players[ packet->UCID ].SkillBonusTimeout = time(nullptr);
}

void RCDL::InsimNPL( struct IS_NPL* packet )
{
    PLIDtoUCID[ packet->PLID ] = packet->UCID;
}

void RCDL::InsimPLP( struct IS_PLP* packet)
{
    memset(&players[PLIDtoUCID[ packet->PLID ]  ].Info, 0, sizeof( CompCar ) );
    PLIDtoUCID.erase( packet->PLID );
}

void RCDL::InsimPLL( struct IS_PLL* packet )
{
    memset(&players[PLIDtoUCID[ packet->PLID ]  ].Info, 0, sizeof( CompCar ) );
    PLIDtoUCID.erase( packet->PLID );
}

void RCDL::InsimCNL( struct IS_CNL* packet )
{
    Save( packet->UCID );
    players.erase( packet->UCID );
}

void RCDL::InsimCPR( struct IS_CPR* packet )
{
    players[ packet->UCID ].PName = packet->PName;
}

void RCDL::InsimMSO( struct IS_MSO* packet )
{
    if (packet->UCID == 0)
        return;

    byte UCID = packet->UCID;

    xString Message = packet->Msg + packet->TextStart;
    vector<xString> args = Message.split(' ',1);

    if(players[UCID].Admin && args.size() > 1)
    {
        if(args[0] == "!lvl_up")
        {
            for (auto& p: players)
            {
                if (players[p.first].UName == args[1])
                {
                    int up = 1;
                    if(args.size() == 3)
                        up = args[2].asInt();

                    players[p.first].LVL += up;
                    insim->SendMTC(255, StringFormat(msg->_(p.first, "GetLvl"), players[ p.first ].PName.c_str(), players[ p.first ].LVL));
                    return;
                }
            }
            insim->SendMTC(UCID, "^2| ^7^C����� �� ������");
        }

        if(args[0] == "!lvl_down")
        {
            for (auto& p: players)
            {
                if (players[p.first].UName == args[1])
                {
                    int up = 1;
                    if(args.size() == 3)
                        up = args[2].asInt();

                    players[p.first].LVL -= up;
                    insim->SendMTC(255, StringFormat(msg->_(p.first, "GetLvl"), players[ p.first ].PName.c_str(), players[ p.first ].LVL));
                    return;
                }
            }
            insim->SendMTC(UCID, "^2| ^7^C����� �� ������");
        }
    }
}

void RCDL::Save (byte UCID)
{
    if(!players[UCID].Loaded)
        return;

    DB_ROW query;
    query["lvl"] = ToString(players[ UCID ].LVL);
    query["skill"] = ToString(players[ UCID ].Skill);
    db->update("dl",query,{{"username",players[ UCID ].UName}});

}

void RCDL::InsimCON( struct IS_CON* packet )
{
    byte UCID = PLIDtoUCID[packet->A.PLID];
    byte UCID1 = PLIDtoUCID[packet->B.PLID];

    players[ UCID ].SkillBonusTimeout = time(nullptr);
    players[ UCID ].SkillCoef = 1;

    players[ UCID1 ].SkillBonusTimeout = time(nullptr);
    players[ UCID1 ].SkillCoef = 1;
}
void RCDL::InsimOBH( struct IS_OBH* packet )
{
    byte UCID = PLIDtoUCID[packet->PLID];
    players[ UCID ].SkillBonusTimeout = time(nullptr);
    players[ UCID ].SkillCoef = 1;
}


void RCDL::InsimMCI( struct IS_MCI* pack_mci )
{
    for (int i = 0; i < pack_mci->NumC; i++)
    {
        byte UCID = PLIDtoUCID[ pack_mci->Info[i].PLID ];

        int X = pack_mci->Info[i].X / 65536;
        int Y = pack_mci->Info[i].Y / 65536;
        int S = (int)pack_mci->Info[i].Speed * 360 / 32768;
        int X1 = players[ UCID ].Info.X / 65536;
        int Y1 = players[ UCID ].Info.Y / 65536;

        if (X1 == 0 && Y1 == 0)
        {
            X1=X;
            Y1=Y;
        }

        float Skill = Distance(X, Y, X1, Y1);

        if (S > 30)
        {
            players[ UCID ].Skill += abs(Skill * 1.5f) * players[ UCID ].SkillCoef;
        }

        players[ UCID ].Info = pack_mci->Info[i];
    }
}

void RCDL::Event()
{
    // ����� � ����� ������
    for ( auto play: players )
    {
        int X = players[ play.first ].Info.X /65536;
        int Y = players[ play.first ].Info.Y /65536;
        int Speed = players[ play.first ].Info.getSpeed();

        byte Count = 0;

        for(auto play2: players)
        {
            if(play2.first == play.first)
                continue;

            int X1 = players[ play2.first ].Info.X /65536;
            int Y1 = players[ play2.first ].Info.Y /65536;
            int Speed1 = players[ play2.first ].Info.getSpeed();

            float Dist = (Speed + Speed1)/20 + 6;

            if(Distance(X,Y, X1,Y1) < 20 && abs(Speed1 - Speed) < Dist && Speed > 5 && Speed1 > 5)
                ++Count;
        }

        if(Count > 4)
            Count = 4;

        if(Count > 1)
        {
            if(play.second.SkillBonusTimeout + 10 < time(nullptr))
                players[ play.first ].SkillCoef = 1.0 + Count*0.25;
        }
        else
        {
            players[ play.first ].SkillBonusTimeout = time(nullptr);
            players[ play.first ].SkillCoef = 1.0;
        }

        float nextlvl = ( pow( players[ play.first ].LVL, 2) * 0.5 + 100 ) * 1000;
        float skl = ( players[ play.first ].Skill / nextlvl ) * 100;

        string Msg = StringFormat(msg->_(play.first, "LvlAndSkill"), players[ play.first ].LVL, skl);

        if(players[ play.first ].SkillCoef > 1)
            Msg = StringFormat(msg->_(play.first, "LvlAndSkill2"), players[ play.first ].LVL, skl, players[ play.first ].SkillCoef);

        insim->SendButton(255, play.first, 1, 100, 5, 30, 4, 32 + 64, Msg);

        if (players[ play.first ].Skill > nextlvl)
        {
            players[play.first].LVL ++;
            players[play.first].Skill = 0;

            insim->SendMTC(255, StringFormat(msg->_(play.first, "GetLvl"), players[ play.first ].PName.c_str(), players[ play.first ].LVL));
        }
    }
}

void
RCDL::SaveAll()
{
    for( auto i: players)
    {
        Save(i.first);
    }
}
