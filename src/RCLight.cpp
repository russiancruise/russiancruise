using namespace std;
#include "RCLight.h"

RCLight*
RCLight::self = nullptr;

RCLight*
RCLight::getInstance()
{
    if(!self)
        self = new RCLight();

    return self;
}
RCLight::RCLight()
{
    ClassName = "RCLight";
}

RCLight::~RCLight()
{

}

bool RCLight::SetLightCop( byte UCID, bool Key)
{
    if (players[UCID].LightCop && !Key)
        for (int f = 121; f < 139; f++)
            insim->SendBFN( UCID, f);

    players[UCID].LightCop = Key;

    return true;
}

int RCLight::init(const char* Dir)
{
    strcpy(RootDir,Dir);
    this->db = DBMySQL::Get();

    this->insim = CInsim::getInstance(); // ����������� ��������� ������� ������

    this->msg = RCMessage::getInstance();

    this->dl = RCDL::getInstance();

    CCText("^3"+ClassName+":\t\t^2inited");
    return 0;
}

void
RCLight::SaveConfig(const char *Track)
{
    char file[MAX_PATH];
    sprintf(file, "%s/RCLight/tracks/%s/Pedestrians.json", RootDir, Track);
    ofstream f;
    f.open(file, ios::out);
    f << configWriter.write( PedestriansConfig );
    f.close();
}

void RCLight::ReadConfig(const char *Track)
{
    config.clear();
    Lights.clear();

    insim->LightSet(255,LIGHT_COLOR_NONE);

    TrackName = Track;
    char file[255];

    ifstream readf;

    // �������� Pedestrians

    sprintf(file, "%s/RCLight/tracks/%s/Pedestrians.json", RootDir, Track);

    readf.open(file, ios::in);

    if(!readf.is_open())
    {
        CCText("  ^7RCLight    ^1ERROR: ^8file " + (string)file + " not found");
        return ;
    }

    bool readed = configReader.parse( readf, PedestriansConfig, false );

    readf.close();

    if ( !readed )
    {

        // report to the user the failure and their locations in the document.
        cout  << "Failed to parse configuration\n"
              << configReader.getFormattedErrorMessages();
        return;
    }

    if(PedestriansConfig.isArray())
    {
        Pedestrians.clear();

        for(auto Pd:PedestriansConfig)
        {
            Pedestrian p;
            memset(&p,0,sizeof(Pedestrian));

            p.startPos.x = Pd["start"]["X"].asInt();
            p.startPos.y = Pd["start"]["Y"].asInt();
            p.startPos.z = Pd["start"]["Z"].asInt();

            p.endPos.x = Pd["end"]["X"].asInt();
            p.endPos.y = Pd["end"]["Y"].asInt();
            p.endPos.z = Pd["end"]["Z"].asInt();

            float xx =  p.startPos.x - p.endPos.x;
            float yy =  p.startPos.y - p.endPos.y;
            float gip = sqrt(xx * xx + yy * yy);
            float ddd = acos(xx / gip) * (180 / M_PI) + 90;

            if(yy < 0)
                ddd = 180.f - ddd;

            ddd = fmod(ddd,360.0f);
            if(ddd < 0)
                ddd += 360.0f;

            ddd=(ddd+180) / 360 * 256; //���� �������� ��� ������� � ������

            p.Heading = byte(ddd);

            Pedestrians.push_back(p);
        }

        //clearPedestrians();
    }
    MaxID = 0;
    insim->SendTiny(TINY_AXM,228);

    CCText("  ^7RCLight\t^2OK");
}

void RCLight::InsimCNL( struct IS_CNL* packet )
{
    players.erase( packet->UCID );
}

void RCLight::InsimCPR( struct IS_CPR* packet )
{
    players[ packet->UCID ].PName = packet->PName;
}

void RCLight::InsimMCI ( struct IS_MCI* pack_mci )
{
    for (int i = 0; i < pack_mci->NumC; i++)
    {
        byte UCID = PLIDtoUCID[ pack_mci->Info[i].PLID ];
        players[UCID].Info = pack_mci->Info[i];

        /** player **/
        int X = pack_mci->Info[i].X / 65536;
        int Y = pack_mci->Info[i].Y / 65536;
        int H = (pack_mci->Info[i].Heading / 182 + 180)%360;
        int D = (pack_mci->Info[i].Direction / 182 + 180)%360;

        int LightID = 0;    // �� ��������� (1-3)

        int arc = 80;       // ������ ��������� (� �������� ����-����� �� ���)

        int j = 0;
        for (auto lgh: Lights)
        {
            if (CheckPosition(4, lgh.X, lgh.Y, X, Y))
            {
                int diffHeading = abs(lgh.Heading - H);
                if (diffHeading > 180)
                    diffHeading = abs(diffHeading - 360);

                if (diffHeading < arc)
                {
                    LightID = lgh.ID;
                    players[UCID].Light = LightID;

                    players[UCID].LightNum = j;
                }
            }
            j++;
        }

        if (LightID > 0)
        {
            Svetofor(UCID, LightID);
        }
        else
        {
            if (players[UCID].Light != 0)
            {
                int diffDirect = abs(Lights[players[UCID].LightNum].Heading - D);
                if (diffDirect > 180)
                    diffDirect = abs(diffDirect - 360);

                if (diffDirect < arc)
                {
                    if (players[UCID].RedLight)
                    {
                        players[UCID].OnRed = true;
                    }
                    else
                    {
                        players[UCID].OnRed = false;
                    }
                }
                /*
                for (int f = 121; f < 139; f++)
                {
                    insim->SendBFN( UCID, f);
                }*/
                players[UCID].Light = 0;
            }
        }

        if (players[UCID].LightCop)
        {
            SvetoforCop( UCID );
        }
    }
}

int RCLight::CheckLight(byte UCID)
{
    if (players[UCID].Light>0)
    {
        if (players[UCID].RedLight)
            return 2;
        else if (players[UCID].GreenLight)
            return 1;
    }
    return 0;
}

bool RCLight::CheckOnRed(byte UCID)
{
    return players[UCID].OnRed;
}

void RCLight::OnRedFalse(byte UCID)
{
    players[UCID].OnRed = false;
}

void RCLight::InsimMSO( struct IS_MSO* packet )
{
    xString Message = packet->Msg + packet->TextStart;

    vector<xString> args = Message.split(' ');

    if (Message == "!light" && players[packet->UCID].Admin)
    {
        if (LightWorks == true)
        {
            LightWorks = false;
            insim->SendMTC(packet->UCID, "^1| ^7^C��������� ^1���������");
        }
        else
        {
            LightWorks = true;
            insim->SendMTC(packet->UCID, "^1| ^7^C��������� ^2��������");
        }
    }

    if(args.size() > 1)
    {
        if(args[0] == "!light" && players[packet->UCID].Admin)
        {
            if(args[1] == "night")
            {
                if (nightTime == true)
                {
                    nightTime = false;
                    insim->SendMTC(packet->UCID, "^1| ^7^C������ ��������� ^1���������");
                }
                else
                {
                    nightTime = true;
                    insim->SendMTC(packet->UCID, "^1| ^7^C������ ��������� ^2��������");
                }
            }
        }
    }

    if (Message == "!addStart" && players[packet->UCID].Admin)
    {
        if(packet->PLID == 0)
            return;

        players[packet->UCID].startPos.x = players[packet->UCID].Info.X / 4096;
        players[packet->UCID].startPos.y = players[packet->UCID].Info.Y / 4096;
        players[packet->UCID].startPos.z = players[packet->UCID].Info.Z / 4096;

        insim->SendMTC(packet->UCID, "^1| ^7^C��������� ����� ���������");
    }

    if (Message == "!addEnd" && players[packet->UCID].Admin)
    {
        if(packet->PLID == 0)
            return;

        if(players[packet->UCID].startPos.x == 0 && players[packet->UCID].startPos.y == 0 && players[packet->UCID].startPos.z == 0)
            return;

        Json::Value point;
        point["start"]["X"] = players[packet->UCID].startPos.x;
        point["start"]["Y"] = players[packet->UCID].startPos.y;
        point["start"]["Z"] = players[packet->UCID].startPos.z;

        point["end"]["X"] = players[packet->UCID].Info.X / 4096;
        point["end"]["Y"] = players[packet->UCID].Info.Y / 4096;
        point["end"]["Z"] = players[packet->UCID].Info.Z / 4096;

        PedestriansConfig.append(point);
        SaveConfig(TrackName.c_str());

        players[packet->UCID].startPos.x = 0;
        players[packet->UCID].startPos.y = 0;
        players[packet->UCID].startPos.z = 0;
        insim->SendMTC(packet->UCID, "^1| ^7^C����� ^2���������");
    }
}

void RCLight::InsimNCN( struct IS_NCN* packet )
{
    if (packet->UCID == 0)
    {
        return;
    }
    players[ packet->UCID ].UName = packet->UName;
    players[ packet->UCID ].PName = packet->PName;
    players[packet->UCID].Admin = packet->Admin;

    if(!packet->Admin)
    {
        players[packet->UCID].Admin = this->isAdmin(packet->UName);
    }
}

void RCLight::InsimNPL( struct IS_NPL* packet )
{
    PLIDtoUCID[ packet->PLID ] = packet->UCID ;
}

void RCLight::InsimPLP( struct IS_PLP* packet)
{
    //PLIDtoUCID.erase( packet->PLID );
}

void RCLight::InsimPLL( struct IS_PLL* packet )
{
    PLIDtoUCID.erase( packet->PLID );
}

bool RCLight::GetOnLight(byte UCID)
{
    if (players[UCID].Light != 0)
        return true;

    return false;
}

void RCLight::Svetofor ( byte UCID, int LightID )
{
    if (LightID == 1)
    {
        if (DisplayLight1.red == 1)
        {
            players[UCID].RedLight = true;
        }
        else
        {
            players[UCID].RedLight = false;
        }

        if (DisplayLight1.yellow == 1)
        {
            players[UCID].RedLight = false;
            players[UCID].GreenLight = false;
        }

        if (DisplayLight1.green == 1)
        {
            players[UCID].GreenLight = true;
        }
    }
    if (LightID == 2)
    {
        if (DisplayLight2.red == 1)
        {
            players[UCID].RedLight = true;
        }
        else
        {
            players[UCID].RedLight = false;
        }

        if (DisplayLight2.yellow == 1)
        {
            players[UCID].RedLight = false;
            players[UCID].GreenLight = false;
        }

        if (DisplayLight2.green == 1)
        {
            players[UCID].GreenLight = true;
        }
    }
    if (LightID == 3)
    {
        if (DisplayLight3.red == 1)
        {
            players[UCID].RedLight = true;
        }
        else
        {
            players[UCID].RedLight = false;
        }

        if (DisplayLight3.yellow == 1)
        {
            players[UCID].RedLight = false;
            players[UCID].GreenLight = false;
        }

        if (DisplayLight3.green == 1)
        {
            players[UCID].GreenLight = true;
        }
    }
}

void RCLight::SvetoforCop ( byte UCID )
{
    //if (police->GetCopRank(UCID) <= 2) return;

    const char* signal1 ="^0�";
    const char* signal2 ="^0�";
    const char* signal3 ="^0�";
    const char* signal11 ="^0�";
    const char* signal12 ="^0�";
    const char* signal13 ="^0�";
    const char* signal21 ="^0�";
    const char* signal22 ="^0�";
    const char* signal23 ="^0�";

    if (DisplayLight1.red == 1)
        signal1 ="^1�";

    if (DisplayLight1.yellow == 1)
        signal2 ="^3�";

    if (DisplayLight1.green == 1)
        signal3 ="^2�";

    if (DisplayLight2.red == 1)
        signal11 ="^1�";

    if (DisplayLight2.yellow == 1)
        signal12 ="^3�";

    if (DisplayLight2.green == 1)
        signal13 ="^2�";

    if (DisplayLight3.red == 1)
        signal21 ="^1�";

    if (DisplayLight3.yellow == 1)
        signal22 ="^3�";

    if (DisplayLight3.green == 1)
        signal23 ="^2�";

    byte id = 140;

    if(MaxID >= 0)
    {
        insim->SendButton(255, UCID, id++, 168, 112, 6, 17, 32, "");    //telo1
        insim->SendButton(255, UCID, id++, 165, 107, 12, 16, 1, signal1);
        insim->SendButton(255, UCID, id++, 165, 112, 12, 16, 1, signal2);
        insim->SendButton(255, UCID, id++, 165, 117, 12, 16, 1, signal3);
    }

    if(MaxID >= 1)
    {
        insim->SendButton(255, UCID, id++, 162, 112, 6, 17, 32, "");    //telo2
        insim->SendButton(255, UCID, id++, 159, 107, 12, 16, 1, signal11);
        insim->SendButton(255, UCID, id++, 159, 112, 12, 16, 1, signal12);
        insim->SendButton(255, UCID, id++, 159, 117, 12, 16, 1, signal13);
    }

    if(MaxID >= 2)
    {
        insim->SendButton(255, UCID, id++, 156, 112, 6, 17, 32, "");    //telo2
        insim->SendButton(255, UCID, id++, 153, 107, 12, 16, 1, signal21);
        insim->SendButton(255, UCID, id++, 153, 112, 12, 16, 1, signal22);
        insim->SendButton(255, UCID, id++, 153, 117, 12, 16, 1, signal23);
    }
}

void RCLight::WrongWay(byte UCID)
{
    insim->SendButton(255, UCID, 140, 0, 0, 200, 200, 0, "^1�");
    insim->SendButton(255, UCID, 141, 25, 26, 150, 150, 3, "-");
}

void RCLight::Event()
{
    struct tm *lt = tools::GetLocalTime();

    int timeFrom = COptions::getInt(this->ClassName, "NightTimeFrom");
    int timeTo = COptions::getInt(this->ClassName, "NightTimeTo");

    if(lt->tm_hour == timeFrom && lt->tm_min == 0 && lt->tm_sec == 0)
        nightTime = true;

    if(lt->tm_hour == timeTo && lt->tm_min == 0 && lt->tm_sec == 0)
        nightTime = false;

    if (!LightWorks || nightTime)
    {
        if (gff)
        {
            DisplayLight1.red = 0;
            DisplayLight1.yellow = 1;
            DisplayLight1.green = 0;

            DisplayLight2.red = 0;
            DisplayLight2.yellow = 1;
            DisplayLight2.green = 0;

            DisplayLight3.red = 0;
            DisplayLight3.yellow = 1;
            DisplayLight3.green = 0;
            gff = false;

            this->LightSet(0,LIGHT_COLOR_YELLOW);
            this->LightSet(1,LIGHT_COLOR_YELLOW);
            this->LightSet(2,LIGHT_COLOR_YELLOW);
        }
        else
        {
            DisplayLight1.red = 0;
            DisplayLight1.yellow = 0;
            DisplayLight1.green = 0;

            DisplayLight2.red = 0;
            DisplayLight2.yellow = 0;
            DisplayLight2.green = 0;

            DisplayLight3.red = 0;
            DisplayLight3.yellow = 0;
            DisplayLight3.green = 0;
            gff = true;

            this->LightSet(0,LIGHT_COLOR_NONE);
            this->LightSet(1,LIGHT_COLOR_NONE);
            this->LightSet(2,LIGHT_COLOR_NONE);
        }
        return;
    }

    float svtime = time(&sstime)%60;

    if (svtime >= 0 && svtime<17)
    {
        DisplayLight1.red = 0;
        DisplayLight1.yellow = 0;
        DisplayLight1.green = 1;

        DisplayLight2.red = 1;
        DisplayLight2.yellow = 0;
        DisplayLight2.green = 0;

        DisplayLight3.red = 0;
        DisplayLight3.yellow = 0;
        DisplayLight3.green = 1;

        this->LightSet(0,LIGHT_COLOR_GREEN);
        this->LightSet(1,LIGHT_COLOR_RED);
        this->LightSet(2,LIGHT_COLOR_GREEN);

    }
    else if (svtime >= 17 && svtime<20)
    {
        if (gff)
        {
            DisplayLight1.red = 0;
            DisplayLight1.yellow = 0;
            DisplayLight1.green = 1;

            this->LightSet(0,LIGHT_COLOR_GREEN);

            gff = false;
        }
        else
        {
            DisplayLight1.red = 0;
            DisplayLight1.yellow = 0;
            DisplayLight1.green = 0;

            this->LightSet(0,LIGHT_COLOR_NONE);

            gff = true;
        }
    }
    else if (svtime == 20)
    {
        gff = false;

        DisplayLight1.red = 0;
        DisplayLight1.yellow = 1;
        DisplayLight1.green = 0;

        DisplayLight2.red = 1;
        DisplayLight2.yellow = 1;
        DisplayLight2.green = 0;

        this->LightSet(0,LIGHT_COLOR_YELLOW);
        this->LightSet(1,LIGHT_COLOR_RED + LIGHT_COLOR_YELLOW);

    }
    else if (svtime >= 23 && svtime<40)
    {
        DisplayLight1.red = 1;
        DisplayLight1.yellow = 0;
        DisplayLight1.green = 0;

        DisplayLight2.red = 0;
        DisplayLight2.yellow = 0;
        DisplayLight2.green = 1;

        this->LightSet(0,LIGHT_COLOR_RED);
        this->LightSet(1,LIGHT_COLOR_GREEN);
    }
    else if (svtime >= 40 && svtime<43)
    {
        if (gff)
        {
            DisplayLight2.red = 0;
            DisplayLight2.yellow = 0;
            DisplayLight2.green = 1;

            DisplayLight3.red = 0;
            DisplayLight3.yellow = 0;
            DisplayLight3.green = 1;

            this->LightSet(1,LIGHT_COLOR_GREEN);
            this->LightSet(2,LIGHT_COLOR_GREEN);

            gff = false;
        }
        else
        {
            DisplayLight2.red = 0;
            DisplayLight2.yellow = 0;
            DisplayLight2.green = 0;

            DisplayLight3.red = 0;
            DisplayLight3.yellow = 0;
            DisplayLight3.green = 0;

            this->LightSet(1,LIGHT_COLOR_NONE);
            this->LightSet(2,LIGHT_COLOR_NONE);

            gff = true;
        }
    }
    else if (svtime >= 43 && svtime < 46)
    {
        gff = false;
        DisplayLight1.red = 1;
        DisplayLight1.yellow = 0;
        DisplayLight1.green = 0;

        DisplayLight2.red = 0;
        DisplayLight2.yellow = 1;
        DisplayLight2.green = 0;

        DisplayLight3.red = 0;
        DisplayLight3.yellow = 1;
        DisplayLight3.green = 0;

        this->LightSet(0,LIGHT_COLOR_RED);
        this->LightSet(1,LIGHT_COLOR_YELLOW);
        this->LightSet(2,LIGHT_COLOR_YELLOW);

        if(svtime == 43)
            setPedestrians(0);
    }
    else if(svtime >= 46 && svtime < 57)
    {
        gff = false;
        DisplayLight1.red = 1;
        DisplayLight1.yellow = 0;
        DisplayLight1.green = 0;

        DisplayLight2.red = 1;
        DisplayLight2.yellow = 0;
        DisplayLight2.green = 0;

        DisplayLight3.red = 1;
        DisplayLight3.yellow = 0;
        DisplayLight3.green = 0;

        this->LightSet(0,LIGHT_COLOR_RED);
        this->LightSet(1,LIGHT_COLOR_RED);
        this->LightSet(2,LIGHT_COLOR_RED);

        if (svtime > 46)
        {
            int percent = int(ceil((10-(56-svtime))/10*100)); // 10 == 56-46

            if(percent != lastPercent)
            {
                lastPercent = percent;
                setPedestrians(percent);
            }
        }

    }
    else if(svtime == 57)
    {
        gff = false;
        DisplayLight1.red = 1;
        DisplayLight1.yellow = 1;
        DisplayLight1.green = 0;

        DisplayLight2.red = 1;
        DisplayLight2.yellow = 0;
        DisplayLight2.green = 0;

        DisplayLight3.red = 1;
        DisplayLight3.yellow = 1;
        DisplayLight3.green = 0;

        this->LightSet(0,LIGHT_COLOR_RED + LIGHT_COLOR_YELLOW);
        this->LightSet(1,LIGHT_COLOR_RED);
        this->LightSet(2,LIGHT_COLOR_RED + LIGHT_COLOR_YELLOW);

        //setPedestrians(100);
    }
    else if(svtime == 58)
        clearPedestrians();
}

void
RCLight::setPedestrians(int percent)
{
    // ���������� ��������� ��������� �������
    int tm = time(&sstime)%60;
    if (timeBlock == tm)
        return;
    timeBlock = tm;

    clearPedestrians();

    if(!allowPedestrians)
        return;

    ObjectInfo obj;
    obj.Index = 254;
    obj.Flags = 5;

    for(unsigned i = 0; i < Pedestrians.size(); ++i)
    {
        Pedestrian p = Pedestrians[i];

        Pedestrians[i].onRoad = true;
        Pedestrians[i].curPos.x = (p.endPos.x - p.startPos.x)*percent/100 + p.startPos.x;
        Pedestrians[i].curPos.y = (p.endPos.y - p.startPos.y)*percent/100 + p.startPos.y;
        Pedestrians[i].curPos.z = (p.endPos.z - p.startPos.z)*percent/100 + p.startPos.z;

        obj.Heading = Pedestrians[i].Heading;
        obj.X = Pedestrians[i].curPos.x;
        obj.Y = Pedestrians[i].curPos.y;
        obj.Zbyte = Pedestrians[i].curPos.z;

        addObjects.push(obj);
    }
    AddObjects();

}

void
RCLight::clearPedestrians()
{
    for(unsigned i = 0; i < Pedestrians.size(); ++i)
    {
        if(!Pedestrians[i].onRoad)
            continue;

        ObjectInfo obj;

        obj.Index = 254;
        obj.Heading = Pedestrians[i].Heading;
        obj.X = Pedestrians[i].curPos.x;
        obj.Y = Pedestrians[i].curPos.y;
        obj.Zbyte = Pedestrians[i].curPos.z;
        obj.Flags = 5;

        delObjects.push(obj);

        Pedestrians[i].onRoad = false;
        Pedestrians[i].curPos.x = 0;
        Pedestrians[i].curPos.y = 0;
        Pedestrians[i].curPos.z = 0;
    }

    DelObjects();
}

void
RCLight::clearAllPedestrians()
{
    for(unsigned i = 0; i < Pedestrians.size(); ++i)
    {
        Pedestrian p = Pedestrians[i];

        ObjectInfo obj;

        obj.Index = 254;
        obj.Heading = Pedestrians[i].Heading;
        obj.Flags = 5;
        for(int j = 0; j<=100; j++)
        {
            obj.X = (p.endPos.x - p.startPos.x)*j/100 + p.startPos.x;;
            obj.Y = (p.endPos.y - p.startPos.y)*j/100 + p.startPos.y;
            obj.Zbyte = (p.endPos.z - p.startPos.z)*j/100 + p.startPos.z;

            delObjects.push(obj);
        }
    }

    DelObjects();
}


void
RCLight::InsimAXM( struct IS_AXM* packet )
{
    if(packet->ReqI == 228)
    {
        for(int i = 0; i < packet->NumO; ++i)
        {
            if(packet->Info[i].Index != 149)
                continue;

            byte id = packet->Info[i].Flags%128;

            if(id < 0 || id > 3)
                continue;

            if(id > 2)
                continue;

            if(packet->Info[i].Flags%128 > MaxID)
                MaxID = id;

            Light lgh;
            memset(&lgh,0,sizeof(Light));
            lgh.ID = id +1;
            lgh.Heading = (packet->Info[i].Heading*360/256 + 180)%360;

            double X = packet->Info[i].X/16;
            double Y = packet->Info[i].Y/16;

            Vector2 v = Vector2(X-12, Y-3).Rotate(lgh.Heading, Vector2(X,Y));
            lgh.X[0] = f2i(v.x);
            lgh.Y[0] = f2i(v.y);

            v = Vector2(X-12, Y+30).Rotate(lgh.Heading, Vector2(X,Y));
            lgh.X[1] = f2i(v.x);
            lgh.Y[1] = f2i(v.y);

            v = Vector2(X+12, Y+30).Rotate(lgh.Heading, Vector2(X,Y));
            lgh.X[2] = f2i(v.x);
            lgh.Y[2] = f2i(v.y);

            v = Vector2(X+12, Y-3).Rotate(lgh.Heading, Vector2(X,Y));
            lgh.X[3] = f2i(v.x);
            lgh.Y[3] = f2i(v.y);

            this->Lights.push_back(lgh);
        }
    }
}

void
RCLight::LightSet(byte Id, byte Color)
{
    insim->LightSet(Id, Color);
    insim->LightSet(Id+5, Color);
}
