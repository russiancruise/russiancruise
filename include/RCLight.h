#ifndef _RC_LIGHT_H // ��������, ����� ���� ����������� ���� ���
#define _RC_LIGHT_H

#include "RCBaseClass.h"
#include "RCMessage.h"  // Messages
#include "RCDrivingLicense.h"

#include "tools.h"      // Check_Pos  etc.

struct LghPlayer: GlobalPlayer
{
    byte    Light;
    bool    LightCop;
    /** misc **/
    bool	RedLight;
    bool	GreenLight;
    bool 	OnRed = false; //������� �� �� �������

    int     LightNum = 0;

    Vec     startPos = {0,0,0};
};

struct Light
{
    byte    ID;
    int     Heading;
    int     X[4];
    int     Y[4];
};

struct DisplayLight
{
    bool red;
    bool yellow;
    bool green;
};

struct Pedestrian
{
    Vec startPos;
    Vec endPos;
    Vec curPos;
    byte Heading;
    bool onRoad = false;
};

class RCLight:public RCBaseClass
{
private:
    RCLight();   // ����������� ������ (�����������)
    ~RCLight();  // ���������� ������ (�����������)
    static RCLight* self;

    string TrackName;
    RCMessage   *msg;   // ����������-��������� �� ����� RCMessage
    RCDL        *dl;

    map< byte, LghPlayer > players;
    byte MaxID = 0;
    vector<Light> Lights;

    bool    LightWorks = true;
    bool    nightTime = false;
    // ��������
    Json::Value PedestriansConfig;
    vector <Pedestrian> Pedestrians;
    float   PedestrianStep=0;
    void setPedestrians(int percent);
    void clearPedestrians();
    void clearAllPedestrians();
    int lastPercent = 0;


    struct DisplayLight DisplayLight1;
    struct DisplayLight DisplayLight2;
    struct DisplayLight DisplayLight3;

    bool gff=false;
    int timeBlock=0;

    time_t sstime;

    void InsimAXM( struct IS_AXM* packet );
    void InsimCNL( struct IS_CNL* packet );   // ����� ���� � �������
    void InsimCPR( struct IS_CPR* packet );   // ����� ��������������
    void InsimMCI( struct IS_MCI* packet );   // ����� � ������� � ����������� � �.�.
    void InsimMSO( struct IS_MSO* packet );   // ����� �������� ���������
    void InsimNCN( struct IS_NCN* packet );   // ����� ����� ����� �� ������
    void InsimNPL( struct IS_NPL* packet );   // ����� ����� �� ������
    void InsimPLL( struct IS_PLL* packet );   // ����� ���� � �������
    void InsimPLP( struct IS_PLP* packet );   // ����� ���� � �����


    void Svetofor ( byte UCID, int LightID );
    void SvetoforCop ( byte UCID );
    void WrongWay ( byte UCID );

    void LightSet(byte Id, byte Color);


public:
    static RCLight* getInstance();

    bool allowPedestrians = true;

    bool SetLightCop(byte UCID,bool Key);
    void Event();

    bool GetOnLight(byte UCID);
    void OnRedFalse(byte UCID);
    bool CheckOnRed(byte UCID);
    int CheckLight(byte UCID);

    int init(const char* Dir);    // classname - ��������� �� ����� RCStreet.
    void ReadConfig(const char *Track); // ������ ������ � ������ "����� ����������"
    void SaveConfig(const char *Track);
};
#endif // #define _RC_STREET_H
