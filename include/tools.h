#ifndef _RC_TOOLS_H
#define _RC_TOOLS_H

using namespace std;

#include <cmath>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif // M_PI

#include <ctime>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <iconv.h>

#include <stdexcept>

// Sleep in ms
#ifdef __linux__
#include <unistd.h>
#define Sleep(a) usleep(a * 1000)
#else
#include <windows.h>
#endif

typedef unsigned char byte;

struct square           // square of destination point
{
    int     X;       // 4 X points
    int     Y;       // 4 Y points
    char    PlaceName[64];  // The name of Destination Point
};

struct place
{
    int     dealX[10];       // 4 X points of  Place
    int     dealY[10];       // 4 Y points of  Place
    int     NumPoints;      // Count of Destination points/ Need for random
    struct  square point[40]; // Destination points.
};

namespace tools
{
    void log(const char *text);
    string convert_encoding(const string& data, const string& from, const string& to);
    string toCP1251(const string& data);
    struct tm * GetLocalTime();
};

struct Vector2
{
    double x;
    double y;
    Vector2();
    Vector2(double x, double y);

    Vector2 Rotate(double a);
    Vector2 Rotate(double a, Vector2 relative);
};

int f2i(double f);

#endif
