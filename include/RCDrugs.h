#ifndef RCDRUGS_H
#define RCDRUGS_H

#include "RCBaseClass.h"
#include "RCMessage.h"
#include "RCDrivingLicense.h"
#include "RCBank.h"
#include "RCPolice.h"
#include "Items.h"

struct DrugPlayer:public GlobalPlayer
{
    string dealerName; // ��� �������, � ���� �������� ��������� �����
    time_t CookTime;
    bool PlayerCooking = false;
    bool CookPassed = false;
    bool CookFail = false;
    bool InTrack = false;
    bool PlayerConsent = false; // �������� ������ �� �������� ����
    string SearchUname;
    bool CopSearch;
    bool CanSearch=false;
    bool distOk;
};

class RCDrugs:public RCBaseClass
{
private:
    RCDrugs();
    ~RCDrugs();
    static RCDrugs* self;

    map <byte, DrugPlayer>players;

    RCMessage *msg;
    RCDL *dl;
    RCBank      *bank;  // ����������-��������� �� ����� RCBank
    RCPolice *police;
    RCStreet *street;
    RCEnergy *nrg;

    Items* items;


    void ReadConfig(const char *Track);

    void    InsimCNL( struct IS_CNL* packet );      // ����� ���� � �������+
    void    InsimCPR( struct IS_CPR* packet );      // ����� ��������������
    void    InsimMCI( struct IS_MCI *packet );       // ���������� � ��������� ������ �� ������+
    void    InsimMSO( struct IS_MSO* packet );      // ����� �������� ���������+
    void    InsimNCN( struct IS_NCN* packet );      // ����� ����� ����� �� ������+
    void    InsimNPL( struct IS_NPL* packet );      // ����� ����� �� ������
    void    InsimPLL( struct IS_PLL* packet );      // ����� ���� � �������
    void    InsimPLP( struct IS_PLP* packet );      // ����� ���� � �����
    void    InsimCON( struct IS_CON* packet );
    void    InsimOBH( struct IS_OBH* packet );

    void    Marshal(Json::Value, bool on);          // ��������� ������� ������

public:
    static RCDrugs* getInstance();
    int init(const char* Dir);
    void Event();
};

#endif // RCDRUGS_H
