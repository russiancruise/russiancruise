// ************************
//       ���������
// ************************
currency		"RUR"
Energy 			"^%d ^LEnergy: %3.2f^K��"
EnergyArrow		"%s ^K��"
LvlAndSkill 	" ^7Drive Level: ^2%d^7, skill: ^2%4.2f"
LvlAndSkill2 	" ^7Drive Level: ^2%d^7, skill: ^2%4.2f x%1.1f"
PitSaveGood 	"^2Garage"
PitSaveNotGood 	"^3City"
401 			"^3Shop"
402 			"^2Cafe"
404 			"^3Bank"
Dist			"^2%4.3f Km"
Cash 			"%d ^7RUR"

ShowCash 		"^7Cash: ^1%d"
ShowLvl			"^7Drive Level: ^1%d^7, skill: ^1%d%%"
ShowCars 		"^2%s ^3%4.0f ^7km"

// ************************
//       ������
// ************************

200 			"^2Cars"
201 			"^2Fines"
202 			"^2About"

TransfersH 		"^1| ^3LAST TRANSFERS:"

Help1			"^1| ^7RUSSIAN CRUISE"
Help2 			"^1| ^7!lang %s - ^3choose your language"
Help3 			"^1| ^7!info (shift+i) - ^3info screen"
Help4 			"^1| ^7!fines - ^3list of fines"
Help5 			"^1| ^7!cars - ^3list of alloved cars"
Help6 			"^1| ^7!save - ^3save your stats"
Help7 			"^1| ^7!show - ^3show your stats"
Help8 			"^1| ^7!pit - ^3tow your self to pits (^71000 ^3RUR)"
Help9 			"^1| ^7!coffee - ^3coffee (^750 ^3RUR)"
Help10 			"^1| ^7!redbull - ^3Redbull (^7100 ^3RUR)"
Help11			"^1| ^7!tofu - ^3order a tofu (^7800 ^3RUR)"
Help12 			"^1| ^7!users - ^3operations with players"
Help13 			"^1| ^7!911 - ^3call Police Officer at the accident"
Help14			"^1| ^7!help - ^3command list"
Help15			"^1| ^7!rent CARID - ^3rent a car"

2400 			"^1| ^7Set voluntary intake restriction not less than^2"
EnergyOver 		"^1| ^7You have run out of power"
2402 			"^1| ^7You have low power"
2403 			"^1| ^7Take coffee (^3!coffee^7) or Redbull (^3!redbull^7)"
2404 			"^1| ^1Take alloved car ^7(^2!cars^7)"

3100 			"^1| ^7Available Cars:"
3101 			"^2| ^7List your fines:"

// ************************
//    ������ �������
// ************************

1000 "Send Money"
1001 "Send a message"

MsgPlFor	"^7For %s"
ItsYou		"^7It's you"

SendMoney	"^5| ^7You sent ^8%s ^7%d ^3RUR."
GetMoney 	"^5| ^8%s ^7sent you ^7%d ^3RUR."
1101 		"^1| ^7you entered an amount greater than what you have in your account."
1102 		"^1| ^7Try select a smaller amount."
MsgFrom  	"^1| ^7From %s ^7: ^1%s"

// ************************
//       �������
// ************************

ShopDialog1 	"^2| Shop"
ShopDialog2 	"^2| ^7!buy XFG/XRG/etc - ^2buy the car"
ShopDialog3 	"^2| ^7!sell XFG/XRG/etc - ^2sell the car"
ShopDialog4 	"^2| ^7!tun ECU/TRB/WHT - ^2install the tuning"
ShopDialog5 	"^2| ^7!untun ECU/TRB/WHT - ^2uninstall the tuning"
1009 			"^1| ^7You can not sell the car when you're sitting in it"
2000 			"^1| ^7You are not in the shop!"

You_dont_install_a	"^1| ^7You don't install a %s"
You_have_already_installed_a	"^1| ^7You have already installed a %s"
You_dont_have	"^1| ^7You don't have %s"

SHOP_BUY_SUCCESS	"^2| ^7You bought a %s for %d RUR"
SHOP_SELL_SUCCESS	"^2| ^7You sold the %s for %d RUR"

SHOP_SELL_USAGE		"^2| ^7Use !sell CARID (ex. XRT)"
SHOP_SELL_YOU_HAVENT	"^2| ^7You don't have this car!"
SHOP_SELL_RENTED	"^2| ^7Can't sold rented  car!"
SHOP_BUY_USAGE		"^2| ^7Use !buy CARID (����. XRT)"
SHOP_BUY_WE_HAVENT	"^2| ^7We don't have this car!"

SHOP_BUY_NEED_MONEY	"^2| ^7Need ^1%d ^7RUR."
SHOP_BUY_ALREADY_HAVE	"^2| ^7You already have this car"
SHOP_RENT_USAGE		"^2| ^7Use !rent CARID (ex. XRT)"
SHOP_RENT_SUCCESS	"^2| ^7Yoy rent %s for %d RUR/km"
Nothing_to_sell	"^1| ^7Nothing to sell"

// ************************
//         ����
// ************************

GetMoneyA		"^5| ^7You have received %d ^3RUR^7"
RemMoneyA		"^5| ^7You have been charged %d ^3RUR^7"

PayDone 		"^2| ^7Fine paid"
NoFine 			"^2| ^7You don't have this Fine"
NoFines 		"^2| ^7You don't have any Fines"
NoOnBank		"^1| ^7You are not in the bank"
NoManyPay		"^1| ^7You don't have money"
FinesListH		"^1| ^7You have ^1%d ^7fines"

BankDialog1		"^5| Bank"
BankDialog2		"^5| ^7!credit info N - ^2information on the credit in the amount of N"
BankDialog3		"^5| ^7!credit N (!credit 50000) - ^2get a credit in the amount of N"
BankDialog4		"^5| ^7!repay - ^2repay the credit"
BankDialog5		"^5| ^7!deposit info N - ^2information on deposit in the amount of N"
BankDialog6		"^5| ^7!deposit N (!deposit 200000) - ^2open a deposit in the amount of N"
BankDialog7		"^5| ^7!withdraw - ^2close a deposit"
BankDialog8		"^5| ^7!pay N (!pay 13) - ^2pay fine ID = N"
BankDialog9		"^1| ^1 ^7%0.0f ^1day left to remove the credit!"
BankDialog10	"^1| ^1Today is the last day for the repayment of the credit!"
BankDialog11	"^1| ^1YOU ARE OVERDUE CREDIT!"
BankDialog12	"^1| ^7Your account is written off credit (%d) + penalty for each missed day"
BankDialog13	"^5| ^7need level: ^15"
BankDialog14	"^5| ^7Error. ^7Indicate the amount of %d ^3RUR ^7prior to %d ^3RUR^7."
BankDialog15	"^5| ^7Credit Information"
BankDialog16	"^5| ^7You have a credit of %d ^3RUR^7. date of issue: %s"
BankDialog17	"^5| ^7The refund amount: %d ^3RUR^7."
BankDialog18	"^5| ^7Time to remove: ^2%0.0f ^7����."
BankDialog19	"^5| ^7Your available credit in the amount of %d ^3RUR ^7prior to %d ^3RUR^7."
BankDialog20	"^5| ^7The credit is ^230 %^7, for no more than ^230^7 days."
BankDialog21	"^5| ^7In case of early redemption will be charged the full amount of the credit + interest."
BankDialog22	"^5| ^7Desired amount: %d ^3RUR^7."
BankDialog23	"^5| ^7The refund amount: %d ^3RUR^7."
BankDialog24	"^5| ^7You cannot take out a credit without closing the contribution."
BankDialog25	"^5| ^7need level: ^15"
BankDialog26	"^5| ^7You have already granted a credit (^3!credit info^7)"
BankDialog27	"^5| ^7Error. ^7Indicate the amount of %d ^3RUR ^7prior to %d ^3RUR^7."
BankDialog28	"^5| ^7credit ^1denied^7."
BankDialog29	"^5| ^7The amount in your account exceeds the amount of the credit is more than half."
BankDialog30	"^5| ^7In a credit ^1denied^7."
BankDialog31	"^5| ^7The amount of your account below -50000 3RUR^7."
BankDialog32	"^5| ^7You will be issued a credit for the amount of %d ^3RUR^7."
BankDialog33	"^5| ^7You don't have credits."
BankDialog34	"^5| ^7Your account is not enough money to repay the credit."
BankDialog35	"^5| ^7You repay the credit."
BankDialog36	"^5| ^7need level: ^120"
BankDialog37	"^5| ^7Error. ^7Specify the amount to %d ^3RUR."
BankDialog38	"^5| Information on deposits"
BankDialog39	"^5| ^7Do you have a contribution in the amount of %d ^3RUR^7. opening date: %s"
BankDialog40	"^5| ^7Now the amount in your account: %d ^3RUR^7 (%d ^3RUR^7 per day)"
BankDialog41	"^5| ^7Deposit term expired, non-interest bearing."
BankDialog42	"^5| ^7The sum of the moment: %d ^3RUR^7 (%d ^3RUR^7 per day)"
BankDialog43	"^5| ^7Prior to the closing of the contribution remains: ^2%0.0f ^7days."
BankDialog44	"^5| ^7You can put the contribution of up to %d ^3RUR."
BankDialog45	"^5| ^7You have opened the contribution of ^215 %^7  per month, for a period of not more than ^230^7 days."
BankDialog46	"^5| ^7The desired amount of the deposit: %d ^3RUR^7."
BankDialog47	"^5| ^7The sum with interest in ^230 ^7days: %d ^3RUR^7 (%d ^3RUR^7 per day)."
BankDialog48	"^5| ^7You can close your contribution is not earlier than ^214 ^7days after the deposit."
BankDialog49	"^5| ^7You can not open a deposit until you repay the credit."
BankDialog50	"^5| ^7need level: ^120"
BankDialog51	"^5| ^7You have already opened contribution (^3!deposit info^7)."
BankDialog52	"^5| ^7Error. ^7Specify the amount to %d ^3RUR."
BankDialog53	"^5| ^7Your account is not enough money to open a deposit."
BankDialog54	"^5| ^7You have opened a contribution in the amount of %d ^3RUR^7."
BankDialog55	"^5| ^7You have no contribution."
BankDialog56	"^5| ^7You closed contribution."
BankDialog57	"^5| ^7The bank receives a penalty fee %d ^3RUR^7."
BankDialog58	"^5| ^1Caution! ^7Has not yet passed 14 days after the deposit."
BankDialog59	"^5| ^7enter ^3!withdraw yes ^7contribution to close. The Bank will receive a penalty fee ^25 %^7."
BankDialog60	"^5| ^7You closed contribution."

// ************************
//          ����
// ************************

4000 			"^3|^7 I'll take you to work"
4001 			"^3|^7 you're not working here"
4002 			"^3|^7 You work not with me, go away"

1600 			"^3|^2 Tofu Fujiwara ^7shop"
1601 			"^3|^2 !deal ^7- take a job"
1602 			"^3|^2 !undeal ^7- leave the job"
FreeEat			"^3| ^2!eat ^7- eat free tofu"

2201 			"^3| ^7Entered an order for tofu, come to the tofu-shop"
2202 			"^7Take Order"

2203 			"^3| ^7There are currently no free workers"
DelTofu 		"^3| ^7Reservation accepted, wait until the tofu is delievred"
UAreDel 		"^3| ^7You have already made ??an order, wait until the tofu is delievred"
1604 			"^3|^7 Your tofu, Sr."
NotLesEn		"^3| ^7Your power must be less than 80%"
NoTofuWhWork	"^3| ^7You can't order a tofu because you work here"

4100 			"^3| ^7You are fired"
4101 			"^3| ^7You have lost the order - you are fired^1!!!"
4102 			"^3| ^7Need car: ^1UF 1000 ^7or ^1SEAZ 11113"
4103 			"^3| ^7Need prefix ^1[^7PIZZA^1] or ^1[^7TOFU^1]"
4104 			"^3| ^7No positions available"

4200 			"^7Deliver order %s, %s"
4201 			"^3| ^7Delivery to player %s"
4202 			"^3| ^7Take first this order"
4203 			"^3| ^7Wait until I call you"
4204 			"^7Order products in the store"
4205 			"^3| ^7Thank you for ordering products"
4210			"^3| ^8%s^7, current order: ^3%s^7, total: ^3%d"
4300 			"^3| ^7Delivered"

4206			"^3| ^7Shop capital: ^7%9.2f ^3RUR"
4207			"^3| ^7Warehouse: water ^3%0.0f ^7l, flour ^3%0.0f ^7kg, vegetables ^3%0.0f ^7kg, cheese ^3%0.0f ^7kg"
4208			"^3| ^7No workers"
4209			"^3| ^7Workers: ^3%d/%d"

4211			"^3| ^7tofu, at our expense"
4212 			"^3| ^7You have worked long enough to receive free meals"
4213 			"^3| ^7You can eat for free in our institution tofu"

// ************************
//         �����
// ************************

TaxiDialog1			"^6| ^3Taxi Radriges"
TaxiDialog2			"^6| ^2!deal ^7- take a job"
TaxiDialog3			"^6| ^2!undeal ^7- leave the job"

TaxiDeal			"^6| ^7You accepted, go to work"
TaxiAlrdUndeal		"^6| ^7You do not work here"
TaxiUndeal			"^6| ^7You're fired!"
TaxiNeedLvl			"^6| ^7Need level: ^120"
TaxiFiredPenalty	"^6| ^7You were punished to %d minutes! do not want to deal with you."
TaxiAlrdWork		"^6| ^7You already hired"
TaxiNeedPref		"^6| ^7Need a nickname with the prefix %s"
TaxiWrongCar		"^6| ^7You can not work on this car"

TaxiStatH			"^6| ^7Statistic taxi work of ^9%s ^7(^9%s^7)"
TaxiStatNoWork		"^6| ^7No Worked"
TaxiStatLine1		"^6| ^7Current work: %d"
TaxiStatLine2		"^6| ^7All accepted: %d"
TaxiStatLine3		"^6| ^7All lost: %d"
TaxiStatNotF		"^6| ^7Player ^9%d ^C^7not found"

TaxiOnStreet		"^6| ^7Stop after %0.0f meters"
TaxiAccept1			"^7Pick up client from %s"
TaxiAccept2			"^7Take the client to %s"
TaxiAccept11		"^6| ^7Pick up client from %s"
TaxiAccept22		"^6| ^7Take the client to %s"
TaxiDead			"^6| ^7You killed your client - ^1FIRED!"
TaxiPll 			"^6| ^7You lost a client"

// ************************
//        �������
// ************************

1002 			"^7Fine"
1003 			"^7Cancel fine"
1004 			"Being the pursuit"
1005 			"Cancel the pursuit"

RideButton 		"^4PURSUIT BEHIND YOU"
RightAndStop 	"^1Keep to the sidelines of the road and stop"
ArestButton		"^1YOU ARE BUSTED"
1702			"^1 are busted"
1706 			"^7away from the pursuit"

ArestedMin		"^2| ^8%s ^1under arrest for ^7%d ^1min"
Reliaved		"^2| ^8%s ^2relieved from arrest"
YouHaveAr		"^2| ^7You're under arrest. Until the end of the arrest remains ^1%d ^7min"
YouUndArest		"^1You're under arrest"

PogonyaOn 		"^2| ^7Pursuit of %s ^7(%s^7)"
PogonyaOff 		"^2| ^7Cancel pursuit of %s ^7(%s^7)"
GiveFine 		"^2| ^8%s ^7wrote you a fine: %s"
GiveFine2		"^1| ^7You have fine: %s"
GiveFine3		"You have fine"
AddFine 		"^2| ^7You fined %s^7 : %s"
DeletedFine 	"^2| ^8%s ^7canceled you a fine: %s"
DelFine			"^2| ^7You canceled a fine %s ^7: %s"
Speeding 		"^2| ^8%s ^7exceeded the speed limit by ^1%d ^3km/h^7, %s"
FinesButton		"Fines"

1703 			"^2| ^7Please set Pursuit in Off mode"
CopFalse 		"^2| ^7You are not a Policeman"
CopTrue 		"^2| ^7You  Policeman"
1302 			"^2| ^7You are not a Policeman"
1303 			"^2| ^7Policeman can not work in a taxi"

3400 			"^1| ^7You have more than 10 penalties. payment of a fine"

2100 			"^2| ^7Sirena Off"
2101 			"^2| ^7Sirena On"
2102			"^2| ^7All traffic police officers are busy"
WherePog		"^2| ^1You cannot call the DPS during the chase"
WhereDtp		"^2| ^1You have already caused DPS wait"
NoDtpWhenSp		"^2| ^1You cannot call the DPS while driving"
Compens 		"^2| ^7You have received compensation in the amount of %0.0f ^3RUR"
CompensPl 		"^2| ^9%s ^7received compensation in the amount of %0.0f ^3RUR"
2106			"^2| ^7Cannot occur more than once in 5 minutes"
2107			"^2| ^7Your application will be reviewed within 5 minutes"
2108			"^2| ^7Do not leave the scene of an accident before the arrival of the inspector DPS"
2109			"^2| ^1You have a call on the accident from %s"
2110			"^2| ^7The queue is full applications"
2111			"^2| ^7You may not use radar"
2112			"^2| ^7You may not use radar while parsing accident"
2113			"^2| ^7You do not have earned over the past change"
2114			"^2| %s ^7shut your bid to the accident"
2115			"^2| ^7Bid ^8%s ^7to the accident closed"
2116			"^2| ^7Do not take more than one application at the same time"
2117			"^2| %s ^7accept your application for consideration by accident"
2118			"^2| %s ^7received a request ^8%s ^7to the accident"
2119			"^2| ^7You can not start a pursuit while parsing accident"
2120			"^2| ^1You missed the call on accident"
2121			"Application is accepted - %s, %s"
2122			"^2Bid ^8%s ^2under consideration - ^1close the"
2123			"Call on the accident received - %s^8, %s"
2124			"%s ^7 - %s, ^1arrested"
RadarOn 		"^2| ^7Radar On"
RadarOff 		"^2| ^7Radar Off"

AddBarier               "^2| %s ^7^Cset a barrier for  %s"
DelBarier               "^2| ^7^CThe barrier is removed"
addBarier		" ^L^1set a barrier"
delBarier		" ^L^1remove barier"


2600 			"^2| ^7Care in spectators during chase. Fine - %d ^3RUR"
2700 			"^2| ^7Care in spectators during chase. Fine - %d ^3RUR"
EndOfRadar		"^2| ^7There are no available radar"

// ************************
//        ������
// ************************

GetLvl 			"^5| ^8%s ^1reached ^3%d ^1level"
1500 			"^1| ^7Bonus: %d ^3RUR"
bonus_button    "^2 %d ^7RUR ^3|^2 %2.1f^K��"
AddSkill		"^1| ^2+ ^3%2.2f%% ^7skill"
RemSkill 		"^1| ^1- ^3%2.2f%% ^7skill"

1801 			"^1| ^7Take coffee (^3!coffee^7) or Redbull (^3!redbull^7)"

2602 			"^1| ^7Care in spectators in the wrong place. Fine - reset bonus"
2702 			"^1| ^7Care in the boxes in the wrong place. Fine - reset bonus"

EnergyFull		"^1| ^7Energy full"
EnergyIsFeeling	"^1| ^7Drink the previous portion"
2001 			"^1| ^7You do not have enough money"
2002 			"^1| ^7You are not in the cafe"
flood 			"^1| ^7Flood! Fine: %d ^3RUR."
swear 			"^1| ^7Swearwords%s. Fine: %d ^3RUR"

2104 			"^2| ^7Need a parameter"
2105 			"^2| ^7Wrong parameter"

3000 			"^1| ^7Your soul is saved"
SAVE_MANY_TIME		"^1| ^7Can't be saved so often"

CONFIG_RELOADED		"^1| ^3Russian Cruise: ^7Config reloaded"
NEED_LVL	        "^2| ^7Need levewl: ^1%d"


ITEMS_RELOADED		"^1| ^3Russian Cruise: ^7Items reloaded"

// ************************
//     �� ������ ���
// ************************

600 	"^7Auto show"
601 	"^7Service"
602 	"^7Buy"
603 	"^7Sell"
604 	"^1Close"
1800 	"^1| ^7You spent all the power!"
1802 	"^1| ^7Also recommend to relax in a cafe"

// ************************
//     ������ ���� 4 �������
// ************************
help_tab	"^2First help"
info_help_1	"^7How to set voluntary intake restriction:"
info_help_2	"^71. Go to pits"
info_help_3	"^72. Create new car settings (if you use default settings)",
info_help_4	"^73. Find ^tInfo^t tab. Press",
info_help_5	"^74. Find the right-hand column of the screen with the sliders",
info_help_6	"^75. Find slider ^tVoluntary Intake Restriction^t",
info_help_7	"^76. Set the slider to 45%%"

//***************************
//        McDrive
//***************************

MdHeader 				"^7Autocafe ^3McDrive"

MdChose1 				"^7Milkshake (^210%^7, 99 ^3RUR^7)"
MdChose2 				"^7Vegetable salad (^220%^7, 199 ^3RUR^7)"
MdChose3 				"^7Burger (^250%^7, 499 ^3RUR^7)"
MdChose4 				"^7French fries (^275%^7, 749 ^3RUR^7)"
MdChose5 				"^7Bizneslanch (^290%^7, 899 ^3RUR^7)"

MdChoose				"^3| ^7Order is accepted, drive to the next window"
MdPay 					"^3| ^7With you %d ^3RUR"
MdTake 					"^3| ^7Here is your order. Have a good trip!"

MdNotChose				"^3| ^7You do not choose"
MdNotPay 				"^3| ^7You have not paid order"

//***************************
//        Evacuator
//***************************

sto_head		"^3| ^2Maintenance Service"
stodeal			"^3| ^2!deal ^7- take a job"
stoundeal		"^3| ^2!undeal ^7- leave the job"
	
EvcAlrdDeal		"^3| ^7Have you hired"
EvcNoJob		"^3| ^7Free no vacancies"
EvcNeedCar		"^3| ^7Need a car: ^1UFR, XFR"
EvcNeedPref		"^3| ^7I need a nickname with the prefix ^3[^1EV�^3]"
EvcDeal			"^3| ^7You received"
EvcUndeal		"^3| ^7You"re fired"
EvcAlrdUndeal	"^3| ^7You"re here not work"
	
EvcCapital		"^3| ^7Capital MS: %9.2f ^3RUR"
EvcNoWorkers	"^3| ^7Workers do not have"
EvcWorkCount	"^3| ^7Workers: %d/%d"
	
EvcCntInPit		"^3| ^7You can not call a tow truck in pits"
EvcCntSpeed		"^3| ^7You can not call evc on the move"
EvcCntCall		"^3| ^7You already called evc, wait for the arrival of the employee"
EvcCntTime		"^3| ^7You can not call evc more than once every 3 minutes"
	
EvcCall			"^3| ^7You call evc, wait for the arrival of the employee"
EvcCall2		"^3| ^7Received application evacuation from ^9%s ^7(%s)"
	
EvcAlrdCall		"^3| ^7This player has already called evc"
EvcPoliceCall	"^3| ^7You call evc to %s"
EvcPoliceCall2	"^3| %s ^7called the evc, wait for the arrival of the employee"
	
EvcWndTitle		"Evacuation service running on the server"
EvcTake			"Turn the car - ^7%d ^3RUR"
EvcRepair		"Turn and repair - ^7%d ^3RUR"
EvcRespawn		"Evacuate the pits - ^7%d ^3RUR"
EvcFastPit		"Instant transition into the pits - ^1%d ^3RUR"
EvcWndCancel	"Cancel"
	
EvcLeave		"^3| ^7The server is no longer evc, your car is sent into the pits"
EvcAuto			"^3| ^7Automatic evacuation"
EvcAuto2		"^3| ^9%s ^7evacuated automatically"
EvcDown			"^3| ^7Evc were unable to process your application, your car free of charge sent to the pits"
EvcDown2		"^3| ^7You missed the application for the evacuation of the car %s ^7at your expense"
	
EvcGetCall		"^7Calling from %s ^7to ^3%s ^7(%s%d m^7) - ^1%02d:%02d"
EvcGetEv		"^2Evacuation %s - ^1%02d:%02d"
EvcGetEv2		"^2Evacuation: "
	
EvtGetTake		"^3| ^7You turned the ^9%s"
EvtGetTake2		"^3| ^9%s ^7turned your car"
EvcGetRepair	"^3| ^7You turned and repaired ^9%s"
EvcGetRepair2	"^3| ^9%s ^7turned and repaired your car"
EvcGetRespawn	"^3| ^7You evacuated ^9%s ^7to the pits"
EvcGetRespawn2	"^3| ^9%s ^7evacuate you to the pits"
EvcGetPolice	"^3| ^7You evacuated ^9%s ^7to the pits"
EvcGetPolice2	"^3| ^9%s ^7evacuate you to the pits"
	
EvcAfkFired		"^3| ^7You"re standing too long without moving. You"re fired."
EvcFine			"^3| ^7Penalty for false alarm evacuation service"
EvcFine2		"^3| ^9%s ^7fined for false alarm evacuation service"

//**********************
// Driving school
//**********************

box-parking				"Box parking"
hill					"Hill"
parallel-parking		"Parallel parking"
chicane					"Chicane"
u-turn					"U turn"

SCHOOL_FAIL				"^6| ^7Wasted"
SCHOOL_WAIT				"^6| ^7Wait about %d minutes"
SCHOOL_SUCCESS			"^6| ^7Congratulations!!! You get Driver's License category ^3%s"

SCHOOL_ALREADY_EXAM		"^6| ^7You are already in the exam"
SCHOOL_ALREADY_GOT		"^6| ^7You are already got drivers license category %s"
SCHOOL_GOTO_EXAM		"^6| ^7Go to the exercise: %s"

SCHOOL_AREA_WELCOME		"^6| ^7Welcome to Driving school."
SCHOOL_AREA_WELCOME2	"^6| ^7Here you can take an exam to obtain a Category %s driver's license"
SCHOOL_AREA_START		"^6| ^3!start ^7- start examination"

SCHOOL_EXERCISE_PREPARE	"^6| ^7Please go to start position then stop!"
SCHOOL_EXERCISE_START	"^6| ^7Start to do an exercise!"

SCHOOL_PANEL_EXERCISE	"^7Exercise: ^3%s"
SCHOOL_PANEL_CHECKPOINT	"^7Checkpoint: ^3%d^7/^3%d"